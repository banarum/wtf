-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

-- Your code here


local isTraceDebug = false

function trace(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10)

	if not(isTraceDebug)then
		return
	end

	if (arg10) then
		print(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10)
		return
	end
	if (arg9) then
		print(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9)
		return
	end
	if (arg8) then
		print(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8)
		return
	end
	if (arg7) then
		print(arg1, arg2, arg3, arg4, arg5, arg6, arg7)
		return
	end
	if (arg6) then
		print(arg1, arg2, arg3, arg4, arg5, arg6)
		return
	end
	if (arg5) then
		print(arg1, arg2, arg3, arg4, arg5)
		return
	end
	if (arg4) then
		print(arg1, arg2, arg3, arg4)
		return
	end
	if (arg3) then
		print(arg1, arg2, arg3)
		return
	end
	if (arg2) then
		print(arg1, arg2)
		return
	end
	if (arg1) then
		print(arg1)
		return
	end
end

_W=display.contentWidth
_H=display.contentHeight

doc_dir = system.DocumentsDirectory
base_dir = system.ResourceDirectory

crypto = require( "crypto" )

require("src.network.networkConsts").init()

DOWNLOAD_REQUESTED = 0
DOWNLOAD_STARTED = 1
DOWNLOAD_SUCCED = 2
DOWNLOAD_FAILED = 3

SYSTEM_ONGOING = 0
SYSTEM_SUSPENDED = 1

tinsert = table.insert


display.setStatusBar(display.HiddenStatusBar )

sclx = _W/1000
scly = _H/1000

scaleGraphics = 0

optionsBuild = "android"

notifications = require( "plugin.notifications" )




local ga = require("src.network.ga")

ga.init({ -- Only initialize once, not in every file
    isLive = false, -- REQUIRED
    testTrackingID = "UA-99878505-1", -- REQUIRED Tracking ID from Google
    debug = true, -- Recomended when starting
})




isSimulator = (system.getInfo( "environment" ) == "simulator")

require("src.libs.eliteScale").init()


options_save_fname = 'wtf'

sounds_arr = {}



mainGroup = display.newGroup()
director = require("src.libs.director").init()

json = require("json")

mime=require("mime")

multipart = require("src.libs.multipartForm")

playfubApi = require("src.network.playfubApi").init()
facebookApi = require("src.network.facebookApi").init()

moneyApi = require("src.dbHelper.moneyApi").init()
currencyApi = require("src.network.currencyApi").init()

banarumApi = require("src.network.banarumApi").init()
superDesign = require("src.libs.superDesign")
oneSignal = require("plugin.OneSignal")
adsLib = require("src.libs.adsLib")
storeApi = require("src.network.storeApi").init()


local pfCombo = require("plugin.playfab.combo")

PlayFabClientApi = pfCombo.PlayFabClientApi
PlayFabServerApi = pfCombo.PlayFabServerApi
PlayFabClientApi.settings.titleId = _G.playfubId
PlayFabServerApi.settings.devSecretKey = "7R4OW438QIDBJU8WBR9BZC3C1BBT7WXTQXYUPM5YCRNF8TX5I1"


local spreadSheedApi = require("src.dbHelper.spreadSheetApi").init()

oneSignal.Init(_G.pushKey,_G.projectNumber, doNothing)

saves_version = 4
saving_allowed = 1

options_debug = true

img = "image/"
sfx = "sfx/"

login_obj = {}



data_obj = 
{
	["refreshCost"] = 1,
	["hintCost"] = 1,
	["choiceCost"] = 3,
	["volume"] = 1,
	["points"] = {{30,4},{60,2},{70,1}}
}

data_obj["reloadFbPhotos"] = true

_G.gameFontText = "Riffic";



centerX = _W/2
centerY = _H/2





local msgs_mc = require("src.libs.eliteMsgs").new();

function findObjByTag(arr,tag)
	for i=1,#arr do
		local obj = arr[i]
		if obj.tag==tag then
			return obj
		end
	end
	
	return nil
end

msgs_mc.x, msgs_mc.y = 0, 220*scaleGraphics;


function set_msgs_location(x,y)
	msgs_mc.x, msgs_mc.y = x, y;
end

function show_msg(txt)
	if(options_debug==true)then
		if (txt~=nil)then
			mainGroup.parent:insert(msgs_mc);
			msgs_mc:show_msg(txt);
		end
	end
end

function loadFile(fname, directory)
	if not directory then directory = system.DocumentsDirectory; end
	local path =  system.pathForFile(fname, directory);
	if (path==nil) then
		return nil
	end
	local file = io.open( path, "r" );
	if (file) then
		local contents = file:read( "*a" );
		io.close(file);
		if(contents and #contents>0)then
			return contents;
		end
	end
	return nil
end

local symbols_dictionary = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",
							"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"}

function clear_word(text)
	local final_txt = ""
	for i=1,#text do
		local _char = text:sub(i,i)
		for j=1,#symbols_dictionary do
			local _elem = symbols_dictionary[j]
			if (_char ==_elem)then
				final_txt = final_txt.._char
			end
		end
	end
	return final_txt
end

function clear_text_from(text,_elem)
	local final_txt = ""
	if text then
		for i=1,#text do
			local _char = text:sub(i,i)
			if (_char ~=_elem or i~=#text)then
				
				final_txt = final_txt.._char
				
			end
		end
	end
	--trace(final_txt)
	return final_txt
end

function copyFile( srcName, srcPath, dstName, dstPath, overwrite )

    local results = false

    local fileExists = system.pathForFile( srcName, srcPath )
    if ( fileExists == nil ) then
        return nil  -- nil = Source file not found
    end

    -- Check to see if destination file already exists
    if not ( overwrite ) then
        if ( fileLib.doesFileExist( dstName, dstPath ) ) then
            return 1  -- 1 = File already exists (don't overwrite)
        end
    end

    -- Copy the source file to the destination file
    local rFilePath = system.pathForFile( srcName, srcPath )
    local wFilePath = system.pathForFile( dstName, dstPath )

    local rfh = io.open( rFilePath, "rb" )
    local wfh, errorString = io.open( wFilePath, "wb" )

    if not ( wfh ) then
        -- Error occurred; output the cause
        trace( "File error: " .. errorString )
        return false
    else
        -- Read the file and write to the destination directory
        local data = rfh:read( "*a" )
        if not ( data ) then
            trace( "Read error!" )
            return false
        else
            if not ( wfh:write( data ) ) then
                trace( "Write error!" )
                return false
            end
        end
    end

    results = 2  -- 2 = File copied successfully!

    -- Close file handles
    rfh:close()
    wfh:close()

    return results
end

function createSubFolder(pre_path,new_path,dir)
	local lfs = require( "lfs" )
	
	dir = dir or system.DocumentsDirectory

	-- Get raw path to documents directory
	local docs_path = system.pathForFile( pre_path, dir )
	
	-- Change current working directory
	local success = lfs.chdir( docs_path )  -- Returns true on success

	local new_folder_path
	local dname = new_path

	if ( success ) then
		lfs.mkdir( dname )
		new_folder_path = lfs.currentdir() .. "/" .. dname
	end
	return success
end

function transitionRemoveSelfHandler(obj)
	if(obj)then
		if(obj.removeSelf)then
			obj:removeSelf();
		end
	end
end

function getOpponent(player1,player2)
	if (player1==getPlayerId())then
		return player2
	end
	return player1
end

function getPlayerId()
	return data_obj["playfub"]["PlayFabId"]
end

currentLayer = 0

function addButtonAction(btn,callback,addListener,layer)
		layer = layer or 0
		local lastScaleX = btn.xScale
		local lastScaleY = btn.yScale
		
		local lastE = nil
		
		local function onRelease(e)
			if (lastE)then
					if (getDD(e,lastE)<(scaleGraphics*10)^2)then
						audio.play( sounds_arr["click"] )
						callback()
					end
				end
			btn:setFillColor(1,1,1)
			btn.xScale = lastScaleX
			btn.yScale = lastScaleY
			
		end
		
		
		addListener(btn,"touch",function (e)
			if (layer~=currentLayer) then
				return
			end
			
			trace("layer",layer,currentLayer)
			
			if (e.phase=="began")then
				if (btn.lYmax and e.y<=btn.lYmax)then
					return
				end
				btn.xScale = lastScaleX*0.9
				btn.yScale = lastScaleY*0.9
				btn:setFillColor(0.7,0.7,0.7)
				
				display.getCurrentStage():setFocus( e.target )
				lastE = e
				
			elseif (e.phase=="moved")then
				if (btn.lYmax and e.y<=btn.lYmax)then
					display.getCurrentStage():setFocus( nil )
					lastE=nil
				end
				if (lastE)then
					if (getDD(e,lastE)>=(scaleGraphics*10)^2)then
						onRelease(e)
						lastE=nil
					end
				end
			else
				
				display.getCurrentStage():setFocus( nil )
				if (lastE==nil)then
					return
				end
				onRelease(e)
				lastE = nil
			end
		end)
	end

function shuffleTable( t )
    local rand = math.random
    assert( t, "shuffleTable() expected a table, got nil" )
    local iterations = #t
    local j
    
    for i = iterations, 2, -1 do
        j = rand(i)
        t[i], t[j] = t[j], t[i]
    end
end

function show_fade_gfx(callback)
	
	
	msgs_mc.alpha = 0
	local fade_in = display.newRect(mainGroup.parent, _W/2, _H/2, _W, _H);
	
	if (director:getCurrHandler() and director:getCurrHandler().onFade)then
		director:getCurrHandler():onFade()
	end
	
	fade_in:setFillColor(0,0,0);
	local speed = 2
	transition.from(fade_in, {time=300/speed, alpha=0, transition=easing.outQuad, onComplete=function(obj)
		callback();
		transition.to(obj, {time=100/speed, alpha=1, onComplete=transitionRemoveSelfHandler});

		local fade_out = display.newRect(mainGroup.parent, _W/2, _H/2, _W, _H);
		fade_out:setFillColor(0,0,0);
		transition.to(fade_out, {time=200/speed, alpha=0, transition=easing.inQuad, onComplete=transitionRemoveSelfHandler});
		msgs_mc:clear()
		msgs_mc.alpha = 1
	end
	});
end

local function factorial(num)
	local count = num
	local sum=1
	for i=1,count do
		sum=sum*i
	end
	trace("factorial"..sum)
	return sum
end

function bolToNumber(val)
	if(val)then
		return 1;
	else
		return 0;
	end
end

function numberToBol(val)
	if(val==0)then
		return false;
	else
		return true;
	end
end

function printData(data)
	trace(json.encode(data))
end

function is_in_rect(bounds,x,y)
	local x1 = bounds.x1
	local y1 = bounds.y1
	local x2 = bounds.x2
	local y2 = bounds.y2
	if (x>=x1)then
		if (y>=y1)then
			if (x<=x2)then
				if (y<=y2)then
					return true
				end
			end
		end
	end
	return false
end

function get_full_time()
	local time = os.time()
	return time;
end

function get_diff_time(time1,time2)
	return os.difftime(time1,time2)
end

function is_time_changed(tag)
	if data_obj.temp.time==nil then
		data_obj.temp.time={}
	end
	if (data_obj.temp.time[tag]==nil)then
		data_obj.temp.time[tag]=os.time()
	end
	
	if (os.difftime(data_obj.temp.time[tag],os.time())~=0)then
		data_obj.temp.time[tag]=os.time()
		return true
	end
	
	return false
end


function widthX(obj)
	local size = obj.width*obj.xScale
	return size
end

function heightY(obj)
	local size = obj.height*obj.yScale
	return size
end

function setScale(obj,scale)
	obj.xScale,obj.yScale=scale,scale
end

function doNothing()
end

function hit_test_rec(mc,w,h,tx,ty)
	if(mc.x and mc.y)then
		if(tx>mc.x-w/2 and tx<mc.x+w/2)then
			if(ty>mc.y-h/2 and ty<mc.y+h/2)then
				return true
			end
		end
	end
	return false
end

function hit_test_rec_ex(mcx,mcy,w,h,tx,ty)
	--if(mcx and mcy)then
		if(tx>mcx-w/2 and tx<mcx+w/2)then
			if(ty>mcy-h/2 and ty<mcy+h/2)then
				return true
			end
		end
	--end
	return false
end


function ResetSaves()
	login_obj = {}
	saveData()
end

function save_prms()
	save_str = json.encode(login_obj)
end

function saveData()
	local path = system.pathForFile( options_save_fname, system.DocumentsDirectory )
	local file = io.open( path, "w+b" )
	save_prms()
	if file then
		save_str = mime.b64(save_str);
		file:write(save_str);
		io.close( file )
		trace("Saving: ok!")
	else
		trace("Saving: fail!")
	end

end

function string_split(str, pat)
	local t = {}
	if (pat == '' or pat == nil) then
	for i=1,#str do
		table.insert(t,string.char(string.byte(str,i)));
	end
	else
	-- NOTE: use {n = 0} in Lua-5.0
	local fpat = "(.-)" .. pat;
	local last_end = 1
	local s, e, cap = str:find(fpat, 1)
	while s do
		if s ~= 1 or cap ~= "" then
			table.insert(t,cap)
		end
		--trace('cap: '..cap)
		last_end = e+1
		s, e, cap = str:find(fpat, last_end)
	end
	if last_end <= #str then
		cap = str:sub(last_end)
		table.insert(t, cap)
	end
	end
	return t
end

function readFromFile(fileName,directory)

	local data = "";
	if (directory == nil)then
		directory = system.DocumentsDirectory;
		
	end

	local path = system.pathForFile( fileName, directory )
	local fileHandle, errorString = io.open( path, "r" );
	if fileHandle then
   -- read all contents of file into a string
	   data = fileHandle:read( "*a" )
	   io.close( fileHandle )
	else
	   trace( "Reason open failed: " .. errorString )
	end

	return data;
end

function getPoints(time)
	for i=1,#data_obj["points"] do
		local obj = data_obj["points"][i];
		if (time<obj[1] or i==#data_obj["points"])then
			return obj[2]
		end
	end
end

trace("points:",getPoints(49))

function sendNotification(userID,data)
	if (isSimulator or true)then
		manualNotificationSend(userID,data)
		return
	end
	local notification = {
		["contents"] = {["en"] = data}
	}
	notification["include_player_ids"] = {userID}
	
	oneSignal.PostNotification(notification,
		function(jsonData)
			--native.showAlert( "DEBUG", "POST OK!!!", { "OK" } )
			local json = require "json"
			trace(json.encode(jsonData))
		end,
		function(jsonData)
			--native.showAlert( "DEBUG", "POST NOT OK!!!", { "OK" } )
			local json = require "json"
			trace(json.encode(jsonData))
		end
	)
end

function manualNotificationSend(userID,data)
	local headers = {}

	headers["Content-Type"] = "application/json;charset=UTF-8"
	
	trace(userID,data)


	local body = json.encode({["app_id"]=_G.pushKey,["include_player_ids"]={userID},["contents"]={["en"]=data}})
	trace(json.encode(body))
	local params = {}
	params.headers = headers
	params.body = body

	network.request( "https://onesignal.com/api/v1/notifications", "POST", function (e) trace(json.encode(e)) end, params )
end

function loadData()
	
	local path = system.pathForFile(options_save_fname, system.DocumentsDirectory);
	local file = io.open( path, "r" );

	if (file) then
		local contents = file:read( "*a" );
		login_obj = mime.unb64(contents);
		login_obj = json.decode(login_obj);
		trace("Loading: ok!");
	else
		trace("Loading: fail!");
	end
end

function perform_download(obj)
	network.download(
		obj._url,
		obj._type,
		obj._listener,
		obj._params,
		obj._filename,
		obj._dir
	)
end

suspended_obj = {};
function onSystem(evt)
    if evt.type == "applicationStart" then
		data_obj.system_status = SYSTEM_ONGOING
			--LoadGameData(true);
    elseif evt.type == "applicationExit" then
      saveData();
  		Runtime:removeEventListener("system", onSystem);
    elseif evt.type == "applicationSuspend" then
		data_obj.system_status = SYSTEM_SUSPENDED
  		trace('SUSPENDED')
		saveData();
    elseif evt.type == "applicationResume" then
		data_obj.system_status = SYSTEM_ONGOING
		oneSignal.ClearAllNotifications()
    end
end
Runtime:addEventListener("system", onSystem);

function show_main_menu()
	show_fade_gfx(function()
  	director:changeScene('src.ui.main.screenMainMenu');
	end)
end

function prepare_time(_time)
	local time_str = ""
	local secs = _time
	local mins = 0
	local hrs = 0
	local secs_str = ""
	local mins_str = ""
	local hrs_str = ""
	if (secs>60)then
		mins = math.floor(secs/60)
		secs = secs - mins*60
	end
	
	if (mins>60)then
		hrs = math.floor(mins/60)
		mins = mins - hrs*60
	end
	
	secs_str = tostring(secs)
	mins_str = tostring(mins)
	hrs_str = tostring(hrs)
	
	if (#secs_str<2)then
		secs_str = "0"..secs_str
	end
	
	if (#mins_str<2)then
		mins_str = "0"..mins_str
	end
	
	time_str = hrs_str..":"..mins_str..":"..secs_str
	return time_str
end

function show_results()
	show_fade_gfx(function()
  	director:changeScene('src.ui.results.screenResults');
	end)
end

function show_login()
	show_fade_gfx(function()
  	director:changeScene('src.ui.login.screenLogin');
	end)
end

function getArrFromString(str)
	local arr = {}
	for i=1,#str do
		table.insert(arr,str:sub(i,i))
	end
	return arr;
end

function getStringFromArr(arr)
	local str = ""
	for i=1,#arr do
		str = str..arr[i]
	end
	return str;
end

function compareTables(tb1,tb2)
	local tb1Str = json.encode(tb1)
	local tb2Str = json.encode(tb2)
	
	local tb1Arr = getArrFromString(tb1Str)
	local tb2Arr = getArrFromString(tb2Str)
	
	table.sort(tb1Arr)
	table.sort(tb2Arr)
	
	tb1Str = getStringFromArr(tb1Arr)
	tb2Str = getStringFromArr(tb2Arr)
	
	local hash1 = getHash(tb1Str)
	local hash2 = getHash(tb2Str)
	
	return hash1==hash2
end

function getHash(str)
	return crypto.digest( crypto.sha1, str )
end

function getShortData(data)
	local shortData = data["shortData"]
	return shortData

end

function show_pass_restore()
	show_fade_gfx(function()
  	director:changeScene('src.ui.login.screenRestorePassword');
	end)
end

function show_cabinet()
	moneyApi:initMoney(10)
	show_fade_gfx(function()
		director:changeScene('src.ui.cabinet.screenCabinet')
	end)
end

function show_answer()
	show_fade_gfx(function()
		director:changeScene('src.ui.game.screenAnswer')
	end)
end

function createGameData()
	local gameData = 
	{
		task={},
		streak=0,
		time = 0,
		points=0,
		state="win"
	}
	return gameData
end

function updateGoldValue(callback)
	showIndicator(true)
	local tempVal = currencyApi:getMoney(function (data)
		showIndicator(false)
		if (data["isError"])then
			show_msg("Error")
		else
			callback(data)
		end
	end)
	callback({["value"]=tempVal, ["offline"]=true})
end

function show_music()
	moneyApi:initMoney(10)
	show_fade_gfx(function()
		director:changeScene('src.ui.game.screenMusic')
	end)
end

function show_songs()
	moneyApi:initMoney(10)
	show_fade_gfx(function()
		director:changeScene('src.ui.game.screenSongs')
	end)
end

function show_hint()
	moneyApi:initMoney(10)
	show_fade_gfx(function()
		director:changeScene('src.ui.game.screenWriteHint')
	end)
end

function show_movie()
	moneyApi:initMoney(10)
	show_fade_gfx(function()
		director:changeScene('src.ui.game.screenMovie')
	end)
end

function show_films()
	moneyApi:initMoney(10)
	show_fade_gfx(function()
		director:changeScene('src.ui.game.screenFilms')
	end)
end

function show_category()
	moneyApi:initMoney(10)
	show_fade_gfx(function()
		director:changeScene('src.ui.game.screenCategory')
	end)
end

function show_settings()
	show_fade_gfx(function()
		director:changeScene('src.ui.cabinet.screenSettings')
	end)
end

activity_indicator = true

function showIndicator(state)
	if activity_indicator ~= state then 
		native.setActivityIndicator( state )
		activity_indicator = state 
	end
end

showIndicator(false)


function show_custom_scene(scene,prm)
	show_fade_gfx(function()
    --eliteSoundsIns:music_play('song'..math.random(1,2));
  	director:changeScene(prm,'src.'..scene);
	end)
end


function getDD(obj1,obj2)
	local dx = obj1.x-obj2.x
	local dy = obj1.y-obj2.y
	local dd = dx*dx+dy*dy
	return dd
end

function string_glue(arr,deli)
	local txt = "";
	for i=1,#arr do
		if (i>1 and deli)then
			txt = txt..deli..tostring(arr[i])
		else
			txt = txt..tostring(arr[i])
		end
	end
	return txt
end

function getGameParam(game,screen,index)
	trace(game,screen,index)
	game = tostring(game)
	login_obj[game] = login_obj[game] or {}
	login_obj[game][screen] = login_obj[game][screen] or {}
	if (login_obj[game][screen][index]==nil)then
		trace("error",json.encode(login_obj[game][screen]))
	end
	return login_obj[game][screen][index] or ""
end

function setGameParam(game,screen,index,param)
	game = tostring(game)
	if (param==nil or index==nil or screen==nil) then
		login_obj[game] = nil
		return
	end
	login_obj[game] = login_obj[game] or {}
	login_obj[game][screen] = login_obj[game][screen] or {}
	login_obj[game][screen][index] = param
end

function deleteFile(file,dir)
	local destDir = dir or system.DocumentsDirectory  -- Location where the file is stored
	local result, reason = os.remove( system.pathForFile( file, destDir ) )

	if result then
	   return true
	end
	return nil
end

function clearGroup(grp)
	while grp and grp.numChildren>0 do
		grp[1]:removeSelf()
	end
end

function string_ridof(txt,deli)
	local str = string_glue(string_split(txt,deli))
	return str
end



local function init()
	sounds_arr["click"] = audio.loadSound( "sfx/buttonClick.mp3" )
	sounds_arr["cash"] = audio.loadSound( "sfx/Cash Sound.mp3" )
	sounds_arr["coin"] = audio.loadSound( "sfx/Game Coin.mp3" )
	sounds_arr["correct"] = audio.loadSound( "sfx/the Correct Answer 01.mp3" )
	sounds_arr["wrong"] = audio.loadSound( "sfx/wrong20.mp3" )
	loadData()

	if (login_obj==nil)then
		ResetSaves()
	end
	if (login_obj.data==nil)then
		login_obj.data = 
		{
			
		}
		
	end
	
	local adsSettings = {
	  ["iPhone"] = {
		["game_over"] = {
		  mediationType = "order", 
		  adType = "interstitial",
		  frequency = 1,
		  keepOrderDuringSession = true,
		  providers = {
			[1] = {
			  providerName = "chartboostplugin",
			  mustBeCached = false,
			}
		  }
		},
		["during_game"] = {
		  mediationType = "order",
		  adType = "banner",
		  frequency = 1,
		  adPosition = "bottom",
		  keepOrderDuringSession = true,
		  providers = {
			[1] = {
			  providerName = "admob",
			  providerFallback = nil
			}
		  }
		}
	  },
	  ["Android"] = {
		["game_over"] = {
		  mediationType = "order", 
		  adType = "interstitial",
		  frequency = 1,
		  keepOrderDuringSession = true,
		  providers = {
			[1] = {
			  providerName = "chartboostplugin",
			  mustBeCached = false,
			}
		  }
		},
		["during_game"] = {
		  mediationType = "order",
		  adType = "banner",
		  frequency = 1,
		  adPosition = "bottom",
		  keepOrderDuringSession = true,
		  providers = {
			[1] = {
			  providerName = "admob",
			  providerFallback = nil
			}
		  }
		}
	  }
	};
	
	local activeAdsProviders = {
	  ["Android"] = {"admob", "chartboostplugin"}, -- possible values are "tapfortap", "admob", "playhaven", "revmob", "chartboost", "iads"
	  ["iPhone"] = {"admob", "chartboostplugin"} --it should include all the ads providers you've put in the adsSettings table
	};
	
	if(adsLib.init)then
		
		adsLib.init(activeAdsProviders, adsSettings);
	end
	
	function IdsAvailable(userID, pushToken)
		trace("PLAYER_ID:" .. userID)
		if (pushToken) then -- nil if user did not accept push notifications on iOS
			trace("PUSH_TOKEN:" .. pushToken)
			data_obj["push_token"] = pushToken
			data_obj["push_id"] = userID
		end
	end
	
	data_obj["short_data"] = {}
	data_obj["opponent"] = {}

	oneSignal.IdsAvailableCallback(IdsAvailable)
	
	spreadSheedApi:requestData()
	
	show_main_menu();
end

init()
