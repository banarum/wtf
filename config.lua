application =
{
 
    notification =
    {
        google = { projectNumber = "251838571748" },
        iphone =
        {
            types =
            {
                "badge", "sound", "alert"
            }
        }
    },
	license =
    {
        google =
        {
            key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA3oC8ROmSq6WEeL65E9la9e30HUGaVaK5/l/+OTBD7WOTD1MogvzwpPd3URN+toJMsm+Xxh8cj8BHuIrv031DJQU0F9meT9cjO7xagb26buDA4sYeUg0r6shNLquX3WYfaWHTETHvKS5IlmZ4HCw5e/pyFBApOk4kWbYHcG9f0zZblJqKialaMF+rFjP499AznlXipukDnVKDLjojw4G8qBqEZRIKBzsfXmLo0rNTfLakkc+zVHzDJF6QhIh3pvUqOa8eLZurTJlthzLlW/jU6dJ2J1h25T8XxV2nS+RlQMKU1CIFc7PCOA7s7Jw0ISO7aQ41qLgptxwDOGH1C2hVqwIDAQAB",
        },
    },
}