module( ..., package.seeall )

function init()
	local o = {}
	local c = {}
	
	if (isSimulator==false)then
		c.facebook = require("plugin.facebook.v4")
	end
	
	c.callback = doNothing
	
	function o:loginUser(callback)
		if (c:checkError(callback))then
			return
		end
		c.callback = callback
		c.facebook.login(c.onFacebookEvent,{ "user_friends"})
	end
	
	function o:getProfilePhoto(fbId,size,reload,callback)
		local function networkListener( event )
			if ( event.isError ) then
				trace( "Network error - download failed: ", event.response )
				
			elseif ( event.phase == "began" ) then
				trace( "Progress Phase: began" )
			elseif ( event.phase == "ended" ) then
				trace( "Displaying response image file" )
				local myImage = display.newImage( event.response.filename, event.response.baseDirectory)
				callback(myImage)
			end
		end
		
		local img = display.newImage(fbId..".jpg",system.TemporaryDirectory)
		
		if (img~=nil and reload~=true)then
			callback(img)
			return
		elseif(reload==true)then
			img:removeSelf()
			img = nil
		end

		network.download(
			"https://graph.facebook.com/"..fbId.."/picture?width="..size.."&height="..size,
			"GET",
			networkListener,
			nil,
			fbId..".jpg",
			system.TemporaryDirectory
		)
	end
	
	function o:getUserData(callback)
		if (c:checkError(callback))then
			return
		end
		c.callback = callback
		c.facebook.request("me","GET")
	end
	
	function o:logout()
		if (c:checkError(doNothing))then
			return
		end
		c.facebook.logout()
		login_obj["fb_name"] = nil
		login_obj["fb_id"] = nil
	end
	
	function o:onFriendsDialog(callback)
		if (c:checkError(callback))then
			return
		end
		c.callback = callback
		c.facebook.showDialog("apprequests", {filter = "APP_USERS", title="Select a Friend", message="I challenge you to a match!",actionType="TURN"})
	end
	
	function o:onInviteDialog(callback)
		if (c:checkError(callback))then
			return
		end
		c.callback = callback
		c.facebook.showDialog("requests", {filter = "APP_NON_USERS", message="I challenge you to a match!"})
	end
	
	function o:onShareDialog(link, title, callback)
		if (c:checkError(callback))then
			return
		end
		c.callback = callback
		c.facebook.showDialog("link", {link = link, title=''})
	end
	
	function c.extractIds(idsString)
		local ids = {}
		local idsArr = string_split(idsString,"&")
		for i=2,#idsArr do
			tinsert(ids,string_split(idsArr[i],"=")[2])
		end
		return ids
	end
	
	function c.onFacebookEvent(e)
	
		--trace(json.prettify(e))
		
		if (e.name=="fbconnect" and e.type=="session")then
			if (e.token)then
				login_obj["fb_token"] = e.token
				login_obj["fb_expiration"] = e.expiration
				c.callback({["message"]="SignIn Successful",["code"]=200})
				return true
			end
		end
		
		if (e.type=="request")then
			if (e.response)then
				local data = json.decode(e.response)
				if (data["name"])then
					login_obj["fb_name"] = data["name"]
					login_obj["fb_id"] = data["id"]
					c.callback({["message"]="Success",["code"]=200})
					return true
				end
				
			end
			
		elseif (e.type=="dialog" ) then
			c.callback(c.extractIds(e.response))
			return true
		end
		
		c.callback({["status"]="Can't SignIn",["code"]=-1})
	end
	
	function c:checkError(callback)
		if (isSimulator)then
			callback({["message"]="Facebook can't be used in Simulator",["code"]=-1})
			return true
		end
		return false
	end
	
	return o
end