module( ..., package.seeall )

function init()
	_G.playfubId = "B882"
	_G.playfubUrl = "https://".._G.playfubId..".playfabapi.com/"
	_G.playfubRegister = "Client/RegisterPlayFabUser"
	_G.playfubSignIn = "Client/LoginWithPlayFab"
	_G.playfubResestPassword = "Client/SendAccountRecoveryEmail"
	_G.playfubFacebookLogin = "Client/LoginWithFacebook"
	_G.playfubUpdateDisplayName = "Client/UpdateUserTitleDisplayName"
	_G.playfubGetAccountInfo = "Client/GetAccountInfo"
	_G.playfubGetCombinedInfo = "Client/GetPlayerCombinedInfo"
	_G.playfubGetExtraInfo = "Server/GetUserAccountInfo"
	_G.playfubHeaders = {["Content-Type"]="application/json"}
	_G.pushKey = "189b0fb4-2ac7-11e5-a7db-df5d7f402845"
	_G.projectNumber = "251838571748"
	_G.facebookAppId = "826689227397695"
end