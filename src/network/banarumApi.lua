module( ..., package.seeall )

function init()
	local o = {}
	local c = {}
	
	c.url = "http://banarum.com:8080/wtf_api"
	c.upload_url = "http://banarum.com:8080/wtf_upload"
	
	c.isDebug = false
	
	function o:sendImage(fileName, fileDir, callback)
		local headers = c:getHeaders()
		c:uploadFile(c.upload_url, fileName, fileDir, "image/jpeg", function (rsp) callback(json.decode(rsp)) end)
	end
	
	function o:getRooms(username,callback)
		local headers = c:getHeaders()
		local body = c:generateBody("rooms",{["username"]=username})
		c:makeRequest(c.url,"POST",body,headers,function (rsp) callback(json.decode(rsp)) end)
	end
	
	function o:requestPrivateRoom(playerName,opponentName,callback)
		local headers = c:getHeaders()
		local body = c:generateBody("private",{["playerName"]=playerName,["opponentName"]=opponentName})
		c:makeRequest(c.url,"POST",body,headers,function (rsp) callback(json.decode(rsp)) end)
	end
	
	function o:sendGameData(gameId,data,username,callback)
		local headers = c:getHeaders()
		local body = c:generateBody("update",{["gameId"]=gameId,["data"]=json.encode(data),["username"]=username})
		c:makeRequest(c.url,"POST",body,headers,function (rsp) callback(json.decode(rsp)) end)
	end
	
	function o:sendShortGameData(gameId,data,callback)
		local headers = c:getHeaders()
		local body = c:generateBody("short",{["gameId"]=gameId,["data"]=json.encode(data)})
		c:makeRequest(c.url,"POST",body,headers,function (rsp) callback(json.decode(rsp)) end)
	end
	
	function o:getGameData(gameId,callback)
		local headers = c:getHeaders()
		local body = c:generateBody("get",{["gameId"]=gameId})
		c:makeRequest(c.url,"POST",body,headers,function (rsp) callback(json.decode(rsp)) end)
	end
	
	function o:deleteRoom(gameId,callback)
		local headers = c:getHeaders()
		local body = c:generateBody("delete",{["gameId"]=gameId})
		c:makeRequest(c.url,"POST",body,headers,function (rsp) callback(json.decode(rsp)) end)
	end
	
	function o:joinRoom(username,callback)
		local headers = c:getHeaders()
		local body = c:generateBody("join",{["username"]=username})
		c:makeRequest(c.url,"POST",body,headers,function (rsp) callback(json.decode(rsp)) end)
	end
	
	function c:generateBody(action,data)
		local body_action = "action="..action
		local body_data = "data="..json.encode(data)
		local body_hash = "hash="..crypto.digest( crypto.sha1, "wtfapihash228"..json.encode(data) )
		local body = body_action.."&"..body_data.."&"..body_hash
		return body
	end
	
	function c:getHeaders()
		local headers = {}
		headers["Content-Type"] = "application/x-www-form-urlencoded"
		headers["Accept-Language"] = "en-US"
		return headers
	end
	
	function c:uploadFile(url, fileName, fileDir, mimeData, callback)	
		multipartForm = multipart.new()
		multipartForm:addFile("photo", system.pathForFile( fileName, fileDir ), mimeData, fileName)
		 
		local params = {}
		params.body = multipartForm:getBody() -- Must call getBody() first!
		params.headers = multipartForm:getHeaders() -- Headers not valid until getBody() is called.
		 
		local function networkListener( event )
			--if (c.isDebug) then
			--	trace(json.encode(event))
			--end
			if ( event.isError ) then
				trace( "Network error!")
				local error_data = 
				{
					["code"] = -1,
					["errorMessage"] = "Connection Error"
				}
				callback(json.encode(error_data))
			else
				callback(event.response)
			end
		end
		 
		network.request(url , "POST", networkListener, params)
	end
	
	function c:makeRequest(url,type,body,headers,callback)
		local function networkListener( event )
			--if (c.isDebug) then
			--	trace(json.encode(event))
			--end
			if ( event.isError ) then
				------------show_msg(json.encode(event))
				--trace( "Network error: ", event.response )
				local error_data = 
				{
					["code"] = -1,
					["errorMessage"] = "Connection Error"
				}
				callback(json.encode(error_data))
			else
				------------show_msg(event.response)
				callback(event.response)
			end
		end
		local params = {}
		params.headers = headers
		params.body = body
		
		--if (c.isDebug) then
		--	trace(body)
		--end
		
		network.request( url, type, networkListener, params )
	end
	
	return o
end
