module( ..., package.seeall )

function init()
	local o = {}
	local c = {}
	
	function o:loginUser(username,pass,callback)
		local requestBody = 
		{
			["TitleId"] = _G.playfubId,
			["Username"] = username,
			["Password"] = pass
		}
		c:makeApiRequest(PlayFabClientApi.LoginWithPlayFab,requestBody,callback)
	end
	
	function o:subtractCurrency(currencyCode, value, callback)
		local requestBody = 
		{
			["VirtualCurrency"] = currencyCode,
			["Amount"] = value
		}
		c:makeApiRequest(PlayFabClientApi.SubtractUserVirtualCurrency, requestBody, callback)
	end
	
	function o:addCurrency(currencyCode, value, callback)
		local requestBody = 
		{
			["VirtualCurrency"] = currencyCode,
			["Amount"] = value
		}
		c:makeApiRequest(PlayFabClientApi.AddUserVirtualCurrency, requestBody, callback)
	end
	
	function o:getInventoryData(callback)
		local requestBody =
		{
			
		}
		c:makeApiRequest(PlayFabClientApi.GetUserInventory, requestBody, callback)
	end
	
	function o:loginFbUser(fbToken,callback)
		local requestBody = 
		{
			["TitleId"] = _G.playfubId,
			["AccessToken"] = fbToken,
			["CreateAccount"] = true
		}
		c:makeApiRequest(PlayFabClientApi.LoginWithFacebook,requestBody,callback)
	end
	
	function o:getFriendsList(callback)
		local requestBody = 
		{
			["IncludeFacebookFriends"] = true
		}
		c:makeApiRequest(PlayFabClientApi.GetFriendsList,requestBody,callback)
	end
	
	function o:getExtendedInfo(playfabId,callback)
		local requestBody =
		{
			["PlayFabId"] = playfabId
		}
		PlayFabServerApi.GetUserAccountInfo(requestBody,function (data) callback(c:getSuccessData(data)) end,function (data) callback(c:getErrorData(data.errorMessage)) end)
	end
	
	function o:getAccountInfo(playfabId,callback)
		local requestBody =
		{
			["PlayFabId"] = playfabId
		}
		c:makeApiRequest(PlayFabClientApi.GetAccountInfo,requestBody,callback)
	end
	
	function o:registerUser(email,username,pass,callback)
		local requestBody = 
		{
			["TitleId"] = _G.playfubId,
			["Username"] = username,
			["Email"] = email,
			["Password"] = pass,
			["RequireBothUsernameAndEmail"] = true,
			["DisplayName"] = username
		}
		c:makeApiRequest(PlayFabClientApi.RegisterPlayFabUser,requestBody,callback)
	end
	
	function o:changeDisplayName(name,callback)
		local requestBody = 
		{
			["DisplayName"] = name
		}
		c:makeApiRequest(PlayFabClientApi.UpdateUserTitleDisplayName,requestBody,callback)
	end
	
	function o:resetPassword(email,callback)
		local requestBody = 
		{
			["TitleId"] = _G.playfubId,
			["Email"] = email
		}
		c:makeApiRequest(PlayFabClientApi.SendAccountRecoveryEmail,requestBody,callback)
	end
	
	function c:getErrorData(errorMessage,code,errorCode)
		return {["code"]=code or -1,["errorCode"]=errorCode or -1,["errorMessage"]=errorMessage}
	end
	
	function c:getSuccessData(data)
		local newData = {}
		newData["code"] = 200
		newData["data"] = data
		return newData
	end
	
	function c:makeApiRequest(call,data,callback)
		call(data,function (_data) callback(c:getSuccessData(_data)) end,function (_data) callback(c:getErrorData(_data.errorMessage,data.code,_data.errorCode)) end)
	end
	
	return o
end
