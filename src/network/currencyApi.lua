module( ..., package.seeall )

function init()
	local o = {}
	local c = {}
	
	function o:getMoney(callback)
		c.key = data_obj["playfub"]["PlayFabId"]
		playfubApi:getInventoryData(function (data)
			if not(c:isError(data)) then
				login_obj[c.key]["money"] = data["data"]["VirtualCurrency"]["GD"]
				callback({["isError"]=false, ["value"]=data["data"]["VirtualCurrency"]["GD"]})
			else
				callback({["isError"]=true})
			end
		end)
		return login_obj[c.key]["money"]
	end
	
	function o:purchase(value, callback)
		o:getMoney(function (data)
			
			if (data["isError"]==false) then
				if (data["value"]>=value)then
					playfubApi:subtractCurrency("GD", value, function (data)
						
						if not(c:isError(data)) then
							callback({["isError"]=false, ["value"]=data["data"]["Balance"]})
						else
							callback({["isError"]=true})
						end
					end)
				else
					callback({["isError"]=true})
				end
			else
				callback({["isError"]=true})
			end
		end)
	end
	
	function o:addMoney(value, callback)
		playfubApi:addCurrency("GD", value, function (data)
			if not(c:isError(data)) then
				callback({["isError"]=false, ["value"]=data["data"]["Balance"]})
			else
				callback({["isError"]=true})
			end
		end)
	end
	
	function c:isError(data)
		if (data["code"]==200)then
			return false
		end
		return true
	end
	
	return o
end
