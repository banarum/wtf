module( ..., package.seeall )

function init()
	local o = {}
	local c = {}
	
	c.isValid = true
	
	c.lastItem = nil
	
	if (system.getInfo( "platform" )=="android") then
		c.store = require( "plugin.google.iap.v3" )
	elseif (system.getInfo( "platform" )=="ios") then
		c.store = require( "store" )
	else
		c.store = require( "store" )
	end
	
	if (system.getInfo("environment")=="simulator") then
		c.isValid = false
	end
	
	c.productsInfo = {}
	
	
	c.products = {
		"coins_pack_tiny",
		"coins_pack_small",
		"coins_pack_medium",
		"coins_pack_huge",
		"ad_remove"
	}
	
	c.onSetCallbacks = {}
	
	c.productsCommonList = {
		["coins_pack_tiny"] = {
			["google"] = "coins_pack_tiny",
			["apple"] = "wtf.pack1"
		},
		["coins_pack_small"] = {
			["google"] = "coins_pack_small",
			["apple"] = "wtf.pack2"
		},
		["coins_pack_medium"] = {
			["google"] = "coins_pack_medium",
			["apple"] = "wtf.pack3"
		},
		["coins_pack_huge"] = {
			["google"] = "coins_pack_huge",
			["apple"] = "wtf.pack4"
		},
		["ad_remove"] = {
			["google"] = "ad_remove",
			["apple"] = "ad_remove"
		},
	}
	
	c.productsData = {
		["coins_pack_tiny"] = {
			isConsumable = true
		},
		
		["coins_pack_small"] = {
			isConsumable = true
		},
		["coins_pack_medium"] = {
			isConsumable = true
		},
		["coins_pack_huge"] = {
			isConsumable = true
		},
		["ad_remove"] = {
			isConsumable = false,
			param = "no_ads"
		},
	}
	
	c.backCompatList = {}
	
	function o:setCallbackOnParam(param, callback)
		c.onSetCallbacks[param] = callback
	end
	
	
	function c:getNamesArr(commonNames, platform)
		local arr = {}
		for i=1,#commonNames do
			tinsert(arr, c.productsCommonList[commonNames[i]][platform])
		end
		return arr
	end
	
	
	
	function c:getBackCompatArr(commonNames, platform)
		local arr = {}
		for i=1,#commonNames do
			arr[c.productsCommonList[commonNames[i]][platform]] = commonNames[i]
			--trace("arr["..c.productsCommonList[commonNames[i]].."[platform]] = "..commonNames[i])
		end
		--trace(json.prettify(arr))
		return arr
	end
	
	
	if (c.isValid)then
		c.backCompatList = c:getBackCompatArr(c.products, c.store.target)
	end
	
	function o:getPrice(id)
		local name = c.productsCommonList[id][c.store.target]
		if (c.productsInfo[name] == nil) then
			return 0
		end
		return c.productsInfo[name]["localizedPrice"]
	end

	function o:purchaseProduct(id, isConsumable, callback)
		local name = c.productsCommonList[id][c.store.target]
		c.lastItem = 
		{
			name = name,
			isConsumable = (isConsumable) and (c.store.target=="google"),
			callback = callback
		}
		c.store.purchase( name )
	end
	
	function o:restorePurchases()
		c.store.restore()
	end
	
	local function transactionListener( event )
		if not ( event.transaction.state == "failed" ) then  -- Successful transaction
			trace("start transaction")
			if (c.lastItem) then
				if not(event and event.transaction and event.transaction.state == "consumed" and c.lastItem.isConsumable==true)then
					c.lastItem.callback()
					if (c.lastItem.isConsumable) then
						trace("consumed")
						c.store.consumePurchase(c.lastItem.name)
					end
					c.lastItem = nil
				end
			end
			
			
			
			local localProductId = event.transaction.productIdentifier
			
			trace("localProductId: "..localProductId)
			
			local productId = c.backCompatList[localProductId]
			
			if (c.onSetCallbacks[productId])then
				c.onSetCallbacks[productId]()
			end
			
			trace("productId: "..productId)
			
			local data = c.productsData[productId]
			
			if (data.isConsumable==false) then
				login_obj[data.param] = true
			end
			--trace( "event.transaction: " .. json.prettify( event.transaction ) )
	 
		else  -- Unsuccessful transaction; output error details
			--trace( event.transaction.errorType )
			--trace( event.transaction.errorString )
		end
	end
	
	local function productListener(event)
		trace("Inapp",json.encode(event))
		for i=1,#event["products"] do
			local id = event["products"][i]["productIdentifier"]
			c.productsInfo[id] = event["products"][i]
		end
	end
	
	c.store.init( transactionListener )
	
	c.store.loadProducts( c:getNamesArr(c.products, c.store.target), productListener )
	
	return o
end
