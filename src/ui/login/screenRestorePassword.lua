module(..., package.seeall);

function new()

	local localGroup = display.newGroup()

	local buttons = {}

	local removed = false

	touch_btns = true

	local gui_mc = display.newGroup()
	localGroup:insert(gui_mc)
	
	local listeners = {}
	
	local function addListener(obj,type,callback)
		local listenerObj = {obj=obj,type=type,callback=callback}
		obj:addEventListener(type,callback)
		table.insert(listeners,listenerObj)
	end

	function localGroup:removeAllListeners()
		if(localGroup._dead~=true)then
			localGroup._dead = true;
		end
		for i=1,#listeners do
			if (listeners[i]~=nil)then
				local listenerObj = listeners[i]
				listenerObj.obj:removeEventListener(listenerObj.type,listenerObj.callback)
				listeners[i] = nil
			end
		end
	end
	
	
	
	local bg = display.newImage(localGroup,"img/Backgrounds/bg.jpg")
	
	bg.x = _W/2
	bg.y = _H/2
	
	if (_W>_H)then
		setScale(bg,_W/bg.width)
	else
		setScale(bg,_H/bg.height)
	end
	
	local function addButtonAction(btn,callback)
		_G.addButtonAction(btn,callback,addListener)
	end
	
	local function onError(data)
		local errorData = data["errorDetails"]
		if (errorData)then
			if (errorData["Email"])then
				show_msg(errorData["Email"][1])
			end
		elseif(data["errorMessage"])then
			show_msg(data["errorMessage"])
		end
		
	end
	
	local function onReset(data)
		showIndicator(false)
		if (data["code"]==200)then
			show_msg("Message sent!")
			emailField.text = ""
			native.setKeyboardFocus( nil )
		else
			trace(json.encode(data))
			onError(data)
		end
	end
	
	
	local instructionsText = "Input your email address.\nYou'll receive an email containing instructions to reset your password."
	
	set_msgs_location(0,scaleGraphics*100)
	
	require("src.libs.superDesign").addCap(localGroup,show_login,addButtonAction)
	
	local instructions = display.newText{ text = instructionsText, font = _G.gameFontText, fontSize = sclx*60, width = sclx*900, height = 0, align = "center"};
	
	local frame = require("src.libs.superDesign").add_border(localGroup, instructions.contentWidth+sclx*50, instructions.contentHeight*1.2);
	
	localGroup:insert(instructions)
	instructions.x, instructions.y = centerX, centerY-150*scly;
	frame.x,frame.y = instructions.x,instructions.y
	instructions:setFillColor(0, 0, 0);

	emailField = native.newTextField(centerX, 0, 700*sclx, 100*sclx);
	emailField.y = instructions.y+heightY(instructions)/2+emailField.height/2+scly*50
	emailField.placeholder = "Your Email Address";
	emailField.inputType = "email";
	localGroup:insert(emailField)
	
	local lostBtn = display.newGroup()
	local lostBg = display.newImage(lostBtn,"img/button_long_blue.png")
	setScale(lostBg,scaleGraphics*0.25)
	local lostTxtBg = display.newText(lostBtn,"Submit",0,0,_G.gameFontText,scaleGraphics*20)
	lostTxtBg:setTextColor(0,0,0)
	lostTxtBg.yScale = 1.3
	local lostTxt = display.newText(lostBtn,"Submit",0,0,_G.gameFontText,scaleGraphics*20)
	lostBtn.x = _W/2
	lostBtn.y = _H/2+scaleGraphics*80
	addButtonAction(lostBg,function () showIndicator(true);playfubApi:resetPassword(emailField.text,onReset); end)
	localGroup:insert(lostBtn)
	
	function localGroup:onFade()
		for i=1,#listeners do
			if (listeners[i]~=nil)then
				local listenerObj = listeners[i]
				listenerObj.obj:removeEventListener(listenerObj.type,listenerObj.callback)
				listeners[i] = nil
			end
		end
		emailField:removeSelf()
	end
	
	return localGroup

end
