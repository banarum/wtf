module(..., package.seeall);

function new()

	local localGroup = display.newGroup()

	local buttons = {}

	local removed = false

	touch_btns = true

	local gui_mc = display.newGroup()
	localGroup:insert(gui_mc)
	
	local listeners = {}
	
	local function addListener(obj,type,callback)
		local listenerObj = {obj=obj,type=type,callback=callback}
		obj:addEventListener(type,callback)
		table.insert(listeners,listenerObj)
	end

	function localGroup:removeAllListeners()
		if(localGroup._dead~=true)then
			localGroup._dead = true;
		end
		for i=1,#listeners do
			if (listeners[i]~=nil)then
				local listenerObj = listeners[i]
				listenerObj.obj:removeEventListener(listenerObj.type,listenerObj.callback)
				listeners[i] = nil
			end
		end
	end
	
	local function addButtonAction(btn,callback)
		_G.addButtonAction(btn,callback,addListener)
	end
	
	
	
	local bg = display.newImage(localGroup,"img/Backgrounds/bg.jpg")
	
	bg.x = _W/2
	bg.y = _H/2
	
	if (_W>_H)then
		setScale(bg,_W/bg.width)
	else
		setScale(bg,_H/bg.height)
	end
	
	local function onSuccess()
		show_cabinet()
	end
	
	local function onError(data)
		local errorData = data["errorDetails"]
		if (errorData)then
			if (errorData["Email"])then
				show_msg(errorData["Email"][1])
			elseif(errorData["Password"])then
				show_msg(errorData["Password"][1])
			elseif(errorData["DisplayName"])then
				show_msg(errorData["DisplayName"][1])
			elseif(errorData["Username"])then
				show_msg(errorData["Username"][1])
				elseif(errorData["RequireBothUsernameAndEmail"])then
				show_msg(errorData["RequireBothUsernameAndEmail"][1])
			end
		elseif(data["errorMessage"])then
			show_msg(data["errorMessage"])
		end
		
	end
	
	local function onAccountInfo(data)
		showIndicator(false)
		if (data["code"]==200)then
			data_obj["playfub"]["account_data"] = data["data"]
			data_obj["login_type"] = "playfub"
			login_obj["login_type"] = "playfub"
			onSuccess()
		else
			onError(data)
		end
	end
	
	local function onRegister(data)
		
		show_msg("Registration...")
		if (data["code"]==200)then
			data_obj["playfub"] = data["data"]
			show_msg("Registration Successfull")
			playfubApi:getAccountInfo(data_obj["playfub"]["PlayFabId"],onAccountInfo)
		else
			showIndicator(false)
			onError(data)
		end
	end
	
	local function onLogin(data)
		if (data["code"]==200)then
			data_obj["playfub"] = data["data"]
			playfubApi:getAccountInfo(data_obj["playfub"]["PlayFabId"],onAccountInfo)
			show_msg("SignIn Successfull")
		elseif (data["errorCode"]==1001)then
			playfubApi:registerUser(login_obj["wtf_mail"],login_obj["wtf_name"],login_obj["wtf_pass"],onRegister)
			show_msg("Username not found")
		else
			showIndicator(false)
			onError(data)
		end
	end
	
	
	set_msgs_location(0,scaleGraphics*80)
	
	require("src.libs.superDesign").addCap(localGroup,show_main_menu,addButtonAction)
	
	function onBtn()
		if (#nameField.text<3 or #passwordField.text<3) then
			return
		end
		
		login_obj["wtf_mail"] = emailField.text
		login_obj["wtf_name"] = nameField.text
		login_obj["wtf_pass"] = mime.b64(passwordField.text)
		showIndicator(true)
		playfubApi:loginUser(login_obj["wtf_name"],login_obj["wtf_pass"],onLogin)
	end
	
	local function textListener( event )
		if (event.phase == "submitted" ) then
			onBtn()
		end
	end
	
	local emailText = display.newText(localGroup, "Email:", 0, 0, _G.gameFontText, 20*scaleGraphics);
	emailText:setFillColor(0, 0, 0);
	emailText.x, emailText.y = centerX, centerY-160*scaleGraphics;
	
	local nameText = display.newText(localGroup, "Username:", 0, 0, _G.gameFontText, 20*scaleGraphics);
	nameText:setFillColor(0, 0, 0);
	nameText.x, nameText.y = centerX, centerY-80*scaleGraphics;
	local passwordText = display.newText(localGroup, "Password:", 0, 0, _G.gameFontText, 20*scaleGraphics);
	passwordText:setFillColor(0, 0, 0);
	passwordText.x, passwordText.y = centerX, centerY;
  
	emailField = native.newTextField(centerX, emailText.y+emailText.contentHeight*.5+10+15, 240*scaleGraphics, 30*scaleGraphics);
	emailField.placeholder = "Your Email Address";
	emailField.inputType = "email";
	localGroup:insert(emailField)
	emailField:addEventListener( "userInput", textListener )
	
	if (login_obj["wtf_mail"])then
		emailField.text = login_obj["wtf_mail"]
	end

	nameField = native.newTextField(centerX, nameText.y+nameText.contentHeight*.5+10+15, 240*scaleGraphics, 30*scaleGraphics);
	nameField.placeholder = "Your Username";
	localGroup:insert(nameField)
	nameField:addEventListener( "userInput", textListener )
	
	if (login_obj["wtf_name"])then
		nameField.text = login_obj["wtf_name"]
	end

	passwordField = native.newTextField(centerX, passwordText.y+passwordText.contentHeight*.5+10+15, 240*scaleGraphics, 30*scaleGraphics);
	passwordField.isSecure = true;
	passwordField.placeholder = "Your Password";
	passwordField:addEventListener( "userInput", textListener )
	localGroup:insert(passwordField)
	
	if (login_obj["wtf_pass"])then
		passwordField.text = mime.unb64(login_obj["wtf_pass"])
	end

	local dy = 30*scaleGraphics;
	nameField.y = nameText.y + dy;
	emailField.y = emailText.y + dy;
	passwordField.y = passwordText.y + dy;
	
	local loginEmailBtn = display.newGroup()
	local loginEmailBg = display.newImage(loginEmailBtn,"img/button_long_green.png")
	setScale(loginEmailBg,scaleGraphics*0.25)
	local loginEmailTxt = display.newImage(loginEmailBtn,"img/text_login.png")
	setScale(loginEmailTxt,scaleGraphics*0.25)
	loginEmailBtn.x = _W/2
	loginEmailBtn.y = _H/2+scaleGraphics*100
	addButtonAction(loginEmailBg, onBtn)
	localGroup:insert(loginEmailBtn)
	
	local lostBtn = display.newGroup()
	local lostBg = display.newImage(lostBtn,"img/button_long_blue.png")
	setScale(lostBg,scaleGraphics*0.2)
	local lostTxtBg = display.newText(lostBtn,"Lost Password?",0,0,_G.gameFontText,scaleGraphics*15)
	lostTxtBg:setTextColor(0,0,0)
	lostTxtBg.yScale = 1.3
	local lostTxt = display.newText(lostBtn,"Lost Password?",0,0,_G.gameFontText,scaleGraphics*15)
	lostBtn.x = _W/2
	lostBtn.y = _H/2+scaleGraphics*180
	addButtonAction(lostBg,show_pass_restore)
	localGroup:insert(lostBtn)

	if (login_obj["autologin"])then
		showIndicator(true)
		playfubApi:loginUser(login_obj["wtf_name"],login_obj["wtf_pass"],onLogin)
	end
	
	
	localGroup.onFade = function()
		for i=1,#listeners do
			if (listeners[i]~=nil)then
				local listenerObj = listeners[i]
				listenerObj.obj:removeEventListener(listenerObj.type,listenerObj.callback)
				listeners[i] = nil
			end
		end
		emailField:removeSelf()
		nameField:removeSelf()
		trace("kek")
		passwordField:removeSelf()
	end
	
	
	return localGroup

end
