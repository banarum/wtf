module(..., package.seeall);

function new()

	local localGroup = display.newGroup()

	local buttons = {}

	local removed = false
	
	local listGroup = display.newGroup()
	
	local gold_bar
	
	--show_msg("Start Init")

	touch_btns = true

	local gui_mc = display.newGroup()
	localGroup:insert(gui_mc)
	
	local listeners = {}
	
	local function addListener(obj,type,callback)
		local listenerObj = {obj=obj,type=type,callback=callback}
		obj:addEventListener(type,callback)
		table.insert(listeners,listenerObj)
	end

	function localGroup:removeAllListeners()
		if(localGroup._dead~=true)then
			localGroup._dead = true;
		end
		for i=1,#listeners do
			if (listeners[i]~=nil )then
				local listenerObj = listeners[i]
				if (listenerObj.obj and listenerObj.obj.removeEventListener)then
					listenerObj.obj:removeEventListener(listenerObj.type,listenerObj.callback)
					listeners[i] = nil
				end
			end
		end
	end
	
	local mDmax
	
	login_obj["autologin"] = true
	
	local onRelease
	
	local function addButtonAction(btn,callback)
		_G.addButtonAction(btn,callback,addListener)
	end
	
	local updateHandler = nil
	
	
	local bg = display.newImage(localGroup,"img/Backgrounds/bg.jpg")
	
	bg.x = _W/2
	bg.y = _H/2
	
	if (_W>_H)then
		setScale(bg,_W/bg.width)
	else
		setScale(bg,_H/bg.height)
	end

	
	set_msgs_location(0,scaleGraphics*80)
	
	--show_msg("2nd step init")
	
	localGroup:insert(listGroup)
	
	local lastUpdate = 0
	
	local function onDelete(data)
		if (data["code"]==200)then
			lastUpdate = 0
			updateHandler()
		else
			show_msg(data["msg"])
		end
	end
	
	
	local function onDeleteRequested(gameId)
		local function onComplete( event )
			if ( event.action == "clicked" ) then
				local i = event.index
				if ( i == 1 ) then
					banarumApi:deleteRoom(gameId,onDelete)
					showIndicator(true)
				elseif ( i == 2 ) then
					
				end
			end
		end

		-- Show alert with two buttons
		local alert = native.showAlert( "Warning", "Do you want to delete game?", { "Yes", "No" }, onComplete )
	end
	
	local barGrp = display.newGroup()
	
	local function setBarPos(num)
		if num>1 then
			num=1
		elseif num<0 then
			num=0
		end
		barGrp.holder.y = -barGrp.bg.height/2+num*barGrp.bg.height
		
	end
	
	local function setListPos(num)
		if num>1 then
			num=1
		elseif num<0 then
			num=0
		end
		local mul = (listGroup.le-(_H-scaleGraphics*25-listGroup.sy))
	
		listGroup.y = ((-num+(listGroup.sy)/mul)*mul)
		
	end
	
	local function onGame(gameId,currentTurn,shortData)
		trace(shortData["state"])
		if (currentTurn==getPlayerId())then
			if (shortData["state"]=="answer")then
				data_obj["current_game"] = gameId
				show_answer()
			else
				data_obj["current_game"] = gameId
				show_category()
			end
		else
			local function onComplete( event )
				if ( event.action == "clicked" ) then
					local i = event.index
					if ( i == 1 ) then
						login_obj["last_nudge"] = get_full_time()
						sendNotification(shortData[currentTurn],data_obj["playfub"]["account_data"]["AccountInfo"]["TitleInfo"]["DisplayName"].." wants you to hurry up!")
					elseif ( i == 2 ) then
						
					end
				end
			end

			-- Show alert with two buttons
			
			if (login_obj["last_nudge"]==nil or get_full_time()-login_obj["last_nudge"]>20) then
				native.showAlert( "Nudge", "Do you want to nudge the player?", { "Yes", "No" }, onComplete)
			else
				native.showAlert( "Nudge", "You can nudge only once in 20 seconds", { "Ok" }, doNothing)
			end
			
			
		end
	end
	
	
	
	local function getGameElement(gameId,user,currentTurn)
		local item = display.newGroup()
		
		local bgRect = display.newRect(item,0,0,scaleGraphics*240*0.8,scaleGraphics*70*0.8)
		
		local border = superDesign.add_border(item,scaleGraphics*240,scaleGraphics*70)
		
		local photoFrame = display.newGroup()
		local photoBg = display.newImage(photoFrame,"img/item/pl_photo.png")
		setScale(photoBg,scaleGraphics*55/photoBg.height)
		photoFrame.x = -scaleGraphics*240/2+widthX(photoBg)/2+scaleGraphics*10
		item:insert(photoFrame)
		
		item.onImage = function (img)
			if (item and item.removeSelf)then
				setScale(img,widthX(photoBg)*0.9/img.width)
				photoFrame:insert(img)
			else
				img:removeSelf()
			end
		end
		
		local deleteIco = display.newImage(item,"img/item/pl_close_button.png")
		setScale(deleteIco,scaleGraphics*0.3)
		deleteIco.x = -scaleGraphics*240/2+scaleGraphics*5
		deleteIco.y = -scaleGraphics*70/2+scaleGraphics*10
		addButtonAction(deleteIco,function () onDeleteRequested(gameId) end)
		
		local tape_grp = display.newGroup()
		local tape = display.newImage(tape_grp,"img/players/pl_tape.png")
		tape_grp.y = -scaleGraphics*70/2/2*0.8
		setScale(tape,scaleGraphics*0.15)
		item:insert(tape_grp)
		tape_grp.x = photoFrame.x+widthX(photoBg)/2+widthX(tape)/2-scaleGraphics
		
		local tapeText = display.newText(tape_grp,"",0,0,"Riffic",scaleGraphics*12)
		
		local turnText = display.newText(item,"",scaleGraphics*100,scaleGraphics*15,"Riffic",scaleGraphics*12)
		
		
		
		turnText:setTextColor(0)
		
		turnText.anchorX = 1
		
		item.setOpponent = function (name)
			tapeText.text = name
		end
		
		item.setTurn = function (name)
			if (currentTurn~=user)then
				turnText.text = "Your Turn"
			else
				turnText.text = "Turn: "..name
			end
		end
		
		local dash = display.newImage(item,"img/players/pl_dash.png")
		dash.x = photoFrame.x+widthX(photoBg)/2+scaleGraphics*4
		dash.anchorX = 0
		dash.xScale = scaleGraphics*0.18
		dash.yScale = scaleGraphics*0.25
		
		bgRect.lYmax = mDmax
		deleteIco.lYmax = mDmax
		
		addButtonAction(bgRect,function ()
			onGame(gameId,currentTurn,item.shortData)
		end)
		
		return item
	end
	
	local function onName(data)
		if data["code"]==200 then
			login_obj.data[data["data"]["UserInfo"]["PlayFabId"]] = data["data"]
		end
	end
	if (login_obj["no_ads"]==nil)then
		adsLib.showAd("during_game");
	end
	
	local function setGameTitle(gameElement,opponentId)
		if (login_obj.data[opponentId])then
			gameElement.setOpponent(login_obj.data[opponentId]["UserInfo"]["TitleInfo"]["DisplayName"])
			gameElement.setTurn(login_obj.data[opponentId]["UserInfo"]["TitleInfo"]["DisplayName"])
			--trace(json.encode(login_obj.data[opponentId]["UserInfo"]))
			if (login_obj.data[opponentId]["UserInfo"]["TitleInfo"]["Origination"]=="Facebook")then
				facebookApi:getProfilePhoto(login_obj.data[opponentId]["UserInfo"]["FacebookInfo"]["FacebookId"],200,false,gameElement.onImage)
			end
			
			return
		end
		playfubApi:getExtendedInfo(opponentId,function (data) onName(data);setGameTitle(gameElement,opponentId) end)
	end
	
	local function setGamesList(gamesData)
		
		clearGroup(listGroup)
		
		for i=1,#gamesData do
			local gameData = gamesData[i]
			local opponentId = gameData["opponent_id"]
			local gameId = gameData["game_id"]
			local currentTurn = gameData["current_turn"]
			local shortData = gameData["short_data"]
			
			data_obj["opponent"][gameId] = opponentId
			
			local gameElement = getGameElement(gameId,opponentId,currentTurn)
			gameElement.shortData = shortData
			gameElement.gameId = gameId
			
			setGameTitle(gameElement,opponentId)
			
			gameElement.y = scaleGraphics*70*(i-1)
			listGroup.le = gameElement.y+gameElement.contentHeight
			listGroup:insert(gameElement)
		end
		onRelease()
	end
	
	
	
	local lastData = nil
	
	
	local function onRoomsData(data)
		showIndicator(false)
		
		--show_msg("Rooms callback fired")
		
		if (data["data"]==nil)then
			return
		end
		
		--trace(json.encode(table.sort(data["data"])),json.encode(table.sort(lastData)))
		--trace(json.encode(table.sort(data["data"])),json.encode(table.sort(lastData)))
		
		--show_msg("Rooms data GOT")
		
		if (compareTables(data["data"],lastData)==false)then
			local gamesData = {}
			data_obj["games_data"] = {}
			for i=1,#data["data"] do
				local game = data["data"][i]
				if (#game["players"]>1)then
					local opponent = getOpponent(game["players"][1],game["players"][2])
					local currentTurn = game["currentTurn"]
					local gameId = game["gameId"]
					trace (json.encode(data))
					local shortData = getShortData(game)
					data_obj["short_data"][gameId] = shortData
					if (shortData[getPlayerId()]==nil)then
						shortData[getPlayerId()]=data_obj["push_id"] or "000"
						banarumApi:sendShortGameData(gameId,shortData,doNothing)
					end
					local obj = {["game_id"]=gameId,["opponent_id"]=opponent,["current_turn"]=currentTurn,["short_data"]=shortData}
					data_obj["games_data"][gameId] = obj
					
					--show_msg("Data GOT")
					
					table.insert(gamesData,obj)
				end
			end
			trace("changed")
			setGamesList(gamesData)
		end
		lastData = data["data"]
	end
	
	updateHandler = function ()
		
		setBarPos(-(listGroup.y-listGroup.sy)/(listGroup.le-(_H-scaleGraphics*25-listGroup.sy)))
		
		--show_msg("Update handler turn")
		
		if (system.getTimer()>lastUpdate+3000)then
			--show_msg("Turn successful")
			lastUpdate = system.getTimer()
			banarumApi:getRooms(getPlayerId(),onRoomsData)
		end
		
	end
	
	
	
	addListener(Runtime,"enterFrame",updateHandler)
	
	--getPlayername()
	
	local function onRandomGame(data)
		showIndicator(false)
		trace(json.encode(data))
		trace("gameId",data["data"]["gameId"])
		if (data["msg"]=="Room created")then
			local gameData = {}
			gameData[getPlayerId()]=data_obj["push_id"] or "000"
			banarumApi:sendShortGameData(data["data"]["gameId"],gameData,doNothing)
		end
		lastUpdate = 0
		updateHandler()
		show_msg(data["msg"])
	end
	
	local function onFriendsData(data,ids)
		showIndicator(false)
		if (data["code"]==200) then
			for i=1,#ids do
				local fabId;
				for j=1,#data["data"]["Friends"] do
					local obj = data["data"]["Friends"][j]
					if (ids[i]==obj["FacebookInfo"]["FacebookId"])then
						fabId = obj["FriendPlayFabId"]
					end
				end
				banarumApi:requestPrivateRoom(getPlayerId(),fabId)
			end
		end
		
	end
	
	local function onFriendsDialog(friendsIds)
		if (#friendsIds==0)then
			return
		end
		showIndicator(true)
		playfubApi:getFriendsList(function (data) onFriendsData(data,friendsIds) end)
	end
	
	local function onInviteDialog(friendsIds)
		print("FRIENDS", json.encode(friendsIds))
		if (#friendsIds==0)then
			return
		end
		if (login_obj["invited"]==nil) then
			login_obj["invited"] = {}
		end
		
		local num = 0
		
		for i=1,#friendsIds do
			if (login_obj["invited"][friendsIds[i]]==nil) then
				login_obj["invited"][friendsIds[i]] = true
				num = num+10
			end
		end
		
		if (num>0) then
			currencyApi:addMoney(num, function () 
				gold_bar:manualUpdate()
			end)
		end
		
	end
	
	listGroup.le = 0
	
	
	onRelease = function ()
		if (listGroup.y>listGroup.sy)then
			transition.to(listGroup,{time=100,y=listGroup.sy})
		end
		
		if (listGroup.y+listGroup.le<_H-scaleGraphics*50 and listGroup.le>_H-listGroup.sy)then
			transition.to(listGroup,{time=100,y=_H-listGroup.sy-listGroup.le+_H/2-scaleGraphics*100})
		elseif (listGroup.le<=_H-listGroup.sy)then
			transition.to(listGroup,{time=100,y=listGroup.sy})
		end
	end
	
	
	local startPt = nil
	local barPt = nil
	
	local function onTouch(e)
		if e.phase=="began" then
			if (e.y>listGroup.sy-scaleGraphics*50 and e.x>_W/2-scaleGraphics*240/2 and e.x<_W/2+scaleGraphics*240/2)then
				startPt = {x=e.x,y=e.y}
			end
			
			if (e.x>_W/2+scaleGraphics*240/2 and e.y>listGroup.sy-scaleGraphics*50 and e.y<barGrp.y+barGrp.bg.height/2)then
				barPt = e
				onTouch({phase="moved",x=e.x,y=e.y})
			end
			listGroup.nsy = listGroup.y
		elseif(e.phase=="moved") then
			if (startPt)then
				if (listGroup.y>listGroup.sy)then
					listGroup.y = listGroup.nsy - (startPt.y - e.y)/math.sqrt(math.abs(listGroup.y-listGroup.sy))
				elseif(listGroup.y+listGroup.le<_H-scaleGraphics*50)then
					listGroup.y = listGroup.nsy - (startPt.y - e.y)/math.sqrt(math.abs(listGroup.y-(_H-listGroup.sy-listGroup.le+_H/2-scaleGraphics*50)))
				else
					listGroup.y = listGroup.nsy - (startPt.y - e.y)
				end
				startPt = {x=e.x,y=e.y}
				listGroup.nsy = listGroup.y
			end
			
			if (barPt)then
				setListPos((e.y-(barGrp.y-barGrp.bg.height/2))/barGrp.bg.height)
			end
			
			
			
			
		else
			startPt = nil
			barPt = nil
			onRelease()
		end
	end
	
	addListener(localGroup,"touch",onTouch)
	
	--show_msg("Loaded callbacks")
	
	
	local coverBg = display.newImage(localGroup,"img/Backgrounds/bg.jpg")
	
	coverBg.x = _W/2
	coverBg.y = _H/2
	
	if (_W>_H)then
		setScale(coverBg,_W/coverBg.width)
	else
		setScale(coverBg,_H/coverBg.height)
	end
	
	local mask = graphics.newMask( "img/bar_w1.png" )
	
	
	
	coverBg:setMask(mask)
	
	coverBg.maskScaleX = 10
	coverBg.maskScaleY = 1
	coverBg.maskX = 0
	
	
	
	require("src.libs.superDesign").addCabinetCap(localGroup,show_settings,doNothing,addButtonAction)
	gold_bar = require("src.libs.superDesign").add_gold_bar(localGroup,function (e) 
		_G.currentLayer = 1
		local shop = require("src.ui.shop.shopWindow").new(addListener, function () gold_bar:manualUpdate() end)
		localGroup:insert(shop)
	end,addButtonAction, function (data)
		if (data["offline"]==true)then
			return
		end
		
		--show_msg("Money updated")
		
		updateHandler()
		lastUpdate = 0
		showIndicator(true)
	end)
	
	
	
	
	local randomBtn = display.newGroup()
	local randomBg = display.newImage(randomBtn,"img/button_long_green.png")
	setScale(randomBg,scaleGraphics*0.25)
	local randomTxt = display.newImage(randomBtn,"img/text_random.png")
	setScale(randomTxt,scaleGraphics*0.25)
	randomBtn.x = _W/2
	randomBtn.y = scaleGraphics*120
	addButtonAction(randomBg,function () showIndicator(true);banarumApi:joinRoom(getPlayerId(),onRandomGame) end)
	localGroup:insert(randomBtn)
	
	local facebookBtn = display.newGroup()
	local facebookBg = display.newImage(facebookBtn,"img/button_long_blue.png")
	setScale(facebookBg,scaleGraphics*0.25)
	local facebookTxt = display.newImage(facebookBtn,"img/text_facebook.png")
	setScale(facebookTxt,scaleGraphics*0.25)
	facebookBtn.x = _W/2
	facebookBtn.y = scaleGraphics*180
	addButtonAction(facebookBg,function()
		
		local function onComplete( event )
			if ( event.action == "clicked" ) then
				local i = event.index
				if ( i == 1 ) then
					facebookApi:onFriendsDialog(onFriendsDialog)
				elseif ( i == 2 ) then
					facebookApi:onInviteDialog(onInviteDialog)
				end
			end
		end

		-- Show alert with two buttons
		local alert = native.showAlert( "Facebook", "Choose Action", { "Play with Friends", "Invite Friends" }, onComplete )
	end)
	localGroup:insert(facebookBtn)
	
	coverBg.maskY = -bg.height/2+(facebookBtn.y+heightY(facebookBtn)/2+scaleGraphics*6)/bg.xScale
	
	--setGamesList({{["opponent_name"]="sergey120xp",["current_turn"]="sergey120xh"},{["opponent_name"]="sergey120xp",["current_turn"]="sergey120xh"},{["opponent_name"]="sergey120xp",["current_turn"]="sergey120xh"},{["opponent_name"]="sergey120xp",["current_turn"]="sergey120xh"}})
	--setGamesList({{["opponent_name"]="sergey120xp",["current_turn"]="sergey120xh"},{["opponent_name"]="sergey120xp",["current_turn"]="sergey120xh"},{["opponent_name"]="sergey120xp",["current_turn"]="sergey120xh"},{["opponent_name"]="sergey120xp",["current_turn"]="sergey120xh"}})
	listGroup.x = _W/2
	if(not(login_obj.data[getPlayerId()]))then
		playfubApi:getExtendedInfo(getPlayerId(),function (data) onName(data); end)
	end
	
	
	mDmax = facebookBtn.y+heightY(facebookBg)/2
	
	
	listGroup.y = facebookBtn.y+heightY(facebookBg)/2+scaleGraphics*50
	listGroup.sy = listGroup.y
	listGroup.nsy = listGroup.y
	
	
	local bgLine = display.newRect(barGrp,0,0,scaleGraphics*10,_H-listGroup.sy-scaleGraphics*50)
	
	bgLine:setFillColor(127/255,51/255,0/255)
	
	local barHolder = display.newImage(barGrp,"img/scr_button.png")
	
	setScale(barHolder,scaleGraphics*0.5)
	
	barGrp.x = _W/2+(_W/2+scaleGraphics*240/2)/2
	
	barGrp.y = listGroup.sy+bgLine.height/2-scaleGraphics*20
	
	barGrp.holder = barHolder
	barGrp.bg = bgLine
	
	local mul = (listGroup.le-(_H-scaleGraphics*25-listGroup.sy))
	
	localGroup:insert(barGrp)
	trace((-0+(listGroup.sy)/mul)*mul)
	
	setBarPos(1)
	
	
	--show_msg("Init complete")
	
	
	return localGroup

end
