module(..., package.seeall);

function new()

	local localGroup = display.newGroup()

	local buttons = {}

	local removed = false

	touch_btns = true

	local gui_mc = display.newGroup()
	localGroup:insert(gui_mc)
	
	local listeners = {}
	
	local function addListener(obj,type,callback)
		local listenerObj = {obj=obj,type=type,callback=callback}
		obj:addEventListener(type,callback)
		table.insert(listeners,listenerObj)
	end
	
	local function addButtonAction(btn,callback)
		_G.addButtonAction(btn,callback,addListener)
	end
	
	local function addBtn(_txt,type,func,y)
		local btn = display.newGroup()
		local bg = display.newImage(btn,"img/button_"..type..".png")
		setScale(bg,scaleGraphics*0.25)
		local txtBg = display.newText(btn,_txt,0,0,_G.gameFontText,scaleGraphics*18)
		txtBg:setTextColor(0,0,0)
		txtBg.yScale = 1.2
		local txt = display.newText(btn,_txt,0,0,_G.gameFontText,scaleGraphics*18)
		btn.x = _W/2
		btn.y = y
		btn.txt = txt
		btn.txtBg = txtBg
		addButtonAction(bg,func)
		localGroup:insert(btn)
		return btn
	end

function localGroup:removeAllListeners()
		if(localGroup._dead~=true)then
			localGroup._dead = true;
		end
		for i=1,#listeners do
			if (listeners[i]~=nil )then
				local listenerObj = listeners[i]
				if (listenerObj.obj and listenerObj.obj.removeEventListener)then
					listenerObj.obj:removeEventListener(listenerObj.type,listenerObj.callback)
					listeners[i] = nil
				end
			end
		end
	end
	
	
	
	
	
	local bg = display.newImage(localGroup,"img/Backgrounds/bg.jpg")
	
	bg.x = _W/2
	bg.y = _H/2
	
	if (_W>_H)then
		setScale(bg,_W/bg.width)
	else
		setScale(bg,_H/bg.height)
	end
	
	set_msgs_location(0,scaleGraphics*80)
	
	require("src.libs.superDesign").addCap(localGroup,show_cabinet,addButtonAction)
	
	local soundGrp = display.newGroup()
	
	local soundImg = display.newImage(soundGrp,"img/Settings/sfx.png")
	setScale(soundImg,scaleGraphics*0.08)
	
	local barBg = display.newImage(soundGrp,"img/Settings/slidebar.png")
	setScale(barBg,scaleGraphics*0.8)
	barBg.y = scaleGraphics*50
	
	local barBall = display.newImage(soundGrp,"img/Settings/slidebutton.png")
	setScale(barBall,scaleGraphics*1.2)
	barBall.y = scaleGraphics*50
	barBall.x = -widthX(barBg)/2+widthX(barBg)*data_obj["volume"]
	
	addListener(barBall,"touch",function (e)
		if e.phase=="began" then
			barBall:setFillColor(0.4)
			display.getCurrentStage():setFocus( e.target )
		elseif e.phase == "moved" then
			if e.x>=_W/2-widthX(barBg)/2 and e.x<=_W/2+widthX(barBg)/2 then
				barBall.x = e.x-_W/2
				
				local k = (e.x-_W/2+widthX(barBg)/2)/widthX(barBg)
				if (k>0.9) then
					k=1
				elseif (k<0.1) then
					k=0
				end
				
				data_obj["volume"] = k
				
				audio.setVolume(data_obj["volume"])
			end
		else
			audio.play(sounds_arr["coin"])
			barBall:setFillColor(1)
			display.getCurrentStage():setFocus( nil )
		end
	end)
	
	soundGrp.y = scaleGraphics*200
	
	soundGrp.x = _W/2
	
	localGroup:insert(soundGrp)
	
	local shopBtn = addBtn("SHOP","long_green",function ()
		_G.currentLayer = 1
		local shop = require("src.ui.shop.shopWindow").new(addListener, doNothing)
		localGroup:insert(shop)
	end,_H/2+scaleGraphics*50)
	
	storeApi:setCallbackOnParam("ad_remove", function() adsLib.removeAd("during_game") end)
	
	local adsBtn = addBtn("REMOVE ADS","long_blue",function () storeApi:purchaseProduct("ad_remove", false,doNothing) end,shopBtn.y+scaleGraphics*60)
	local restore = addBtn("RESTORE IAP","long_blue",function() storeApi:restorePurchases()	end,adsBtn.y+scaleGraphics*60)
	local logout = addBtn("LOGOUT","red",function() show_main_menu();facebookApi:logout();login_obj["autologin"]=false end,restore.y+scaleGraphics*50)
	logout.txt.size = scaleGraphics*14
	logout.txtBg.size = scaleGraphics*14
	
	
	
	return localGroup

end
