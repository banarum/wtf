module(..., package.seeall);

function new(addListener,onPurchaseCallback)

	

	local boxes = {
	  {"img/menu6/m6_coins_1.png", sclx*246*1.5, sclx*102*1.5, centerX, -400*sclx*1.5, 10},
	  {"img/menu6/m6_coins_2.png", sclx*246*1.5, sclx*102*1.5, centerX, -200*sclx*1.5, 10},
	  {"img/menu6/m6_coins_3.png", sclx*246*1.5, sclx*102*1.5, centerX, 0*sclx*1.5, 10},
	  {"img/menu6/m6_coins_4.png", sclx*246*1.5, sclx*102*1.5, centerX, 200*sclx*1.5, 10},
	};

	local items = {
		{coins=20, cost=0.99, id = "coins_pack_tiny"},
		{coins=50, cost=1.99, id = "coins_pack_small"},
		{coins=100, cost=2.99, id = "coins_pack_medium"},
		{coins=200, cost=4.99, id = "coins_pack_huge"},
	}
	
	local localGroup = display.newGroup()
	
	local rect = display.newRect(localGroup,_W/2,_H/2,_W,_H)
	rect:setFillColor(0,0,0,0.6)

	local boxesGroup = display.newGroup();
  localGroup:insert(boxesGroup);
  for i = 1, #boxes do
	local item_obj = items[i];
	local item = display.newGroup()
	
	
	
	local box = display.newImage(item,"img/menu6/m6_bg.png")
	setScale(box,scly/1.8*800/box.width)
	
	local good_bg = display.newImage(item,"img/menu6/m6_button.png")
	good_bg.xScale = scly/1.8*200/good_bg.width
	good_bg.yScale = scly/1.8*200/good_bg.height
	good_bg.x = -widthX(box)/2+widthX(good_bg)/2+scly/1.8*20-scly/1.8*5
	
	
	local good = display.newImage(item,boxes[i][1])
	setScale(good,scly/1.8*180/good.width)
	good.x = -widthX(box)/2+widthX(good)/2+scly/1.8*20
	
	local price_mc = display.newGroup()
	local price_bg = display.newImage(price_mc,"img/menu6/m6_top.png")
	setScale(price_bg,scly/1.8*150/price_bg.height)
	
	display.newText({parent = price_mc, text = storeApi:getPrice(item_obj.id), x = 0, y = 0, font = _G.gameFontText, fontSize = scaleGraphics*14});
	
	
	
	local txt = display.newText({ text = item_obj.coins, x = good.x, y = scly*20, font = _G.gameFontText, fontSize = scly/1.8*48});
	local txt_rect = display.newRect(item,txt.x,txt.y,txt.width+scly/1.8*10,txt.height+scly*1)
	txt_rect:setFillColor(0,0,0,0.5)
	item:insert(txt)
	txt:setFillColor(1,1,1,1)
	
	price_mc.x = widthX(box)/2-widthX(price_bg)/2-scly/1.8*20
	price_mc.y =heightY(box)/2-heightY(price_bg)/2-scly*5
	
	item:insert(price_mc)
	
	item.x = _W/2
	item.y = heightY(box)*(i-1)
	
	addButtonAction(box,function ()
		storeApi:purchaseProduct(item_obj.id, true, function ()
			local callback
			local counter = 1
			callback = function (data)
				if (data["isError"])then
					print("Connection Error Retry "..counter.." time")
					currencyApi:addMoney(item_obj.coins, callback)
					counter = counter+1
				else
					print("Callback ready")
					if (onPurchaseCallback)then
						onPurchaseCallback()
					end
				end
			end
			
			currencyApi:addMoney(item_obj.coins, callback)
			
		end)
		localGroup:removeSelf()
		_G.currentLayer = 0
	end, addListener, 1)
	
	addListener(rect, "touch", function (e)
		if (e.phase~="began" and e.phase~="moved")then
			localGroup:removeSelf()
			_G.currentLayer = 0
			if (onPurchaseCallback)then
				onPurchaseCallback()
			end
		end
	end)
	
	boxesGroup:insert(item)
end
	
   
  boxesGroup.anchorChildren = true;
  boxesGroup.x, boxesGroup.y = _W/2, _H/2;
	
	
	return localGroup

end
