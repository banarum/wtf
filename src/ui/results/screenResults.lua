module(..., package.seeall);

function new()

	local localGroup = display.newGroup()

	local buttons = {}

	local removed = false

	touch_btns = true

	local gui_mc = display.newGroup()
	localGroup:insert(gui_mc)
	
	local listeners = {}
	
	
	
	local function addListener(obj,type,callback)
		local listenerObj = {obj=obj,type=type,callback=callback}
		obj:addEventListener(type,callback)
		table.insert(listeners,listenerObj)
	end

	function localGroup:removeAllListeners()
		if(localGroup._dead~=true)then
			localGroup._dead = true;
		end
		for i=1,#listeners do
			if (listeners[i]~=nil)then
				local listenerObj = listeners[i]
				listenerObj.obj:removeEventListener(listenerObj.type,listenerObj.callback)
				listeners[i] = nil
			end
		end
	end
	
	local function addButtonAction(btn,callback)
		_G.addButtonAction(btn,callback,addListener)
	end
	
	local function onLoadData(data)
		showIndicator(false)
		if (data["code"]==200)then
			data_obj["currentGameData"] = data["data"]
			trace(data_obj["currentGameData"])
			if (data_obj["currentGameData"].state==nil)then
				data_obj["currentGameData"]=createGameData()
			end
		else
			show_msg("Connection lost")
			show_cabinet()
		end
	end
	
	local function onLinkGot(data)
		if (data and data["url"])then
			local winTxt = "haven't guessed"
			if (data_obj["isWin"])then
				winTxt = "guessed"
			end
			facebookApi:onShareDialog(data["url"], "I "..winTxt.." wtf", doNothing)
		end
		showIndicator(false)
	end
	
	local bg = display.newRect(localGroup,0,0,_W,_H)
	bg.anchorX = 0
	bg.anchorY = 0
	bg.alpha = 0.1
	
	set_msgs_location(0,scaleGraphics*80)
	
	if (data_obj["resultsData"]==nil)then
		showIndicator(true)
		banarumApi:getGameData(data_obj["current_game"],onLoadData)
	end
	
	if not(login_obj["no_ads"])then
		adsLib.showAd("game_over")
	end
	
	
	local frame_mc = display.newGroup()
	local top_mc = display.newGroup()
  
	local frame = require("src.libs.superDesign").add_border(top_mc, sclx*900, sclx*825);
	frame_mc.x,frame_mc.y = _W/2,_H/2-scly*80
	frame_mc.alpha = 0
	
	win_cond = data_obj["isWin"]
  
	local result_grp = display.newGroup()
	local result_img = "img/menu7/m7_top_lose.png"
	local result_text = "LOSE"
	if (win_cond == true)then
		result_img = "img/menu7/m7_top_win.png"
		result_text = "WIN"
	else
		timer.performWithDelay(500,function ()show_msg("The answer was: "..data_obj["answer"]) end )
	end
	
	
	trace("answer: ",data_obj["answer"])
	local result_bg = display.newImage(result_grp,result_img)
	setScale(result_bg,sclx*400/result_bg.width)
	
	display.newText({parent = result_grp, text = result_text, x = 0, y = 0, font = _G.gameFontText, fontSize = sclx*70});
	
	result_grp.y = -sclx*825/2+sclx*15
	
	top_mc:insert(result_grp)
	
	local stats_group = display.newGroup()
	
	local stats_bg = display.newImage(stats_group,"img/menu7/m7_bg.png")
	stats_bg.xScale = sclx*850/stats_bg.width
	stats_bg.yScale = sclx*850/stats_bg.width*0.95
	stats_group.y = -sclx*165+sclx*10
	
	local time_mc = display.newGroup()
	
	local time_bg = display.newImage(time_mc,"img/menu7/m7_line.png")
	time_bg.xScale = sclx*745/time_bg.width
	time_bg.yScale = sclx*63/time_bg.height
	
	local time_ico = display.newImage(time_mc,"img/menu7/m7_clock.png")
	setScale(time_ico,sclx*100/time_ico.width)
	time_ico.x = -sclx*600/2
	
	local time_text = display.newImage(time_mc,"img/menu7/text_time.png")
	setScale(time_text,sclx*60/time_text.height)
	time_text.anchorX = 0
	time_text.x = -sclx*135-widthX(time_text)/2
	
	local time_value = display.newText({parent = time_mc, text = prepare_time(data_obj["resultsData"].time), x = sclx*200, y = 0, font = _G.gameFontText, fontSize = sclx*60,align = "right"});
	time_value:setFillColor(70/255,89/255,111/255)
	time_value.x = sclx*350-time_value.width/2
	time_mc.y = -sclx*75
	
	stats_group:insert(time_mc)
	
	
	local points_mc = display.newGroup()
	
	local points_bg = display.newImage(points_mc,"img/menu7/m7_line.png")
	points_bg.xScale = sclx*745/points_bg.width
	points_bg.yScale = sclx*63/points_bg.height
	
	local points_ico = display.newImage(points_mc,"img/menu7/m7_star.png")
	setScale(points_ico,sclx*100/points_ico.width)
	points_ico.x = -sclx*600/2
	
	local points_text = display.newImage(points_mc,"img/menu7/text_points.png")
	setScale(points_text,sclx*60/points_text.height)
	points_text.anchorX = 0
	points_text.x = -sclx*135-widthX(time_text)/2
	
	local points_value = display.newText({parent = points_mc, text = data_obj["resultsData"].points, x = sclx*200, y = 0, font = _G.gameFontText, fontSize = sclx*60,align = "right"});
	points_value:setFillColor(70/255,89/255,111/255)
	points_value.x = sclx*350-points_value.width/2
	
	points_mc.y = sclx*50
	
	stats_group:insert(points_mc)
	
	top_mc:insert(stats_group)
  
	local strike_txt = display.newText({parent = top_mc, text = "WINNING STREAK", y = sclx*50, font = _G.gameFontText, fontSize = sclx*60,align = "center"});
	strike_txt:setFillColor(70/255,89/255,111/255)
	
	local strike_mc = display.newGroup()
	
	local tape = display.newImage(strike_mc,"img/menu7/m7_tape.png")
	setScale(tape,sclx*380/tape.width)
	
	local tape_txt = display.newText({parent = strike_mc, text = data_obj["resultsData"].streak, font = _G.gameFontText, fontSize = sclx*90,align = "center"});
	
	local photo_1_mc = display.newGroup()
	
	local photo_1 = display.newImage(photo_1_mc,"img/menu7/pl_photo.png")
	setScale(photo_1,sclx*230/photo_1.width)
	photo_1_mc.x = -sclx*280
	
	strike_mc:insert(photo_1_mc)
	
	photo_1_mc.onImage = function (img)
		if (photo_1_mc and photo_1_mc.removeSelf)then
			setScale(img,widthX(photo_1)*0.9/img.width)
			photo_1_mc:insert(img)
		else
			img:removeSelf()
		end
	end
	
	local photo_2_mc = display.newGroup()
	
	local photo_2 = display.newImage(photo_2_mc,"img/menu7/pl_photo.png")
	setScale(photo_2,sclx*230/photo_2.width)
	photo_2_mc.x = sclx*280
	
	photo_2_mc.onImage = function (img)
		if (photo_2_mc and photo_2_mc.removeSelf)then
			setScale(img,widthX(photo_2)*0.9/img.width)
			photo_2_mc:insert(img)
		else
			img:removeSelf()
		end
	end
	
	strike_mc:insert(photo_2_mc)
	
	strike_mc.y = sclx*225
	
	top_mc:insert(strike_mc)
	
	local bottom_grp = display.newGroup()
	local cross_btn = display.newImage(bottom_grp,"img/menu7/m7_button_menu.png")
	setScale(cross_btn,sclx*220/cross_btn.width)
	local true_scl = cross_btn.xScale
	cross_btn.x = -sclx*260
	
	local fb_btn = display.newImage(bottom_grp,"img/menu7/m7_button_share.png")
	setScale(fb_btn,sclx*220/fb_btn.width)
	
	local next_btn = display.newImage(bottom_grp,"img/menu7/m7_button_next.png")
	setScale(next_btn,sclx*220/next_btn.width)
	next_btn.x = sclx*260
	if (data_obj.from_menu==true)then
		next_btn.fill.effect = "filter.grayscale"
		next_btn.blocked = true
		next_btn.alpha = 0
	end
	
	data_obj.from_menu = false
	
	addButtonAction(next_btn,function ()
		if (next_btn.blocked==nil)then
			show_category()
		end
	end)
	
	addButtonAction(fb_btn,function ()
		if (fb_btn.blocked==nil)then
			local function saveWithDelay()
				display.save(top_mc, "results.jpg", system.TemporaryDirectory)
				banarumApi:sendImage("results.jpg", system.TemporaryDirectory, onLinkGot)
			end
			showIndicator(true)
			timer.performWithDelay( 100, saveWithDelay )
		end
	end)
	
	addButtonAction(cross_btn,function ()
		if (next_btn.blocked==nil)then
			show_cabinet()
		end
	end)
	
	if (login_obj.data[getPlayerId()]["UserInfo"]["TitleInfo"]["Origination"]=="Facebook")then
		facebookApi:getProfilePhoto(login_obj.data[getPlayerId()]["UserInfo"]["FacebookInfo"]["FacebookId"],200,false,photo_1_mc.onImage)
	end
	
	if (login_obj.data[data_obj["resultsData"].opponentId]["UserInfo"]["TitleInfo"]["Origination"]=="Facebook")then
		facebookApi:getProfilePhoto(login_obj.data[data_obj["resultsData"].opponentId]["UserInfo"]["FacebookInfo"]["FacebookId"],200,false,photo_2_mc.onImage)
	end
	
	
	
	bottom_grp.y = sclx*500
	frame_mc:insert(top_mc)
	frame_mc:insert(bottom_grp)
  
  transition.to(frame_mc,{delay = 200,time = 200,alpha = 1})
  localGroup:insert(frame_mc)
	
	
	
	return localGroup

end
