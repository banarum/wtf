module(..., package.seeall);

function new()

	local localGroup = display.newGroup()

	local buttons = {}

	local removed = false

	touch_btns = true

	local gui_mc = display.newGroup()
	localGroup:insert(gui_mc)
	
	local listeners = {}
	
	local function addListener(obj,type,callback)
		local listenerObj = {obj=obj,type=type,callback=callback}
		obj:addEventListener(type,callback)
		table.insert(listeners,listenerObj)
	end

	
	function localGroup:removeAllListeners()
		if(localGroup._dead~=true)then
			localGroup._dead = true;
		end
		for i=1,#listeners do
			if (listeners[i]~=nil )then
				local listenerObj = listeners[i]
				if (listenerObj.obj and listenerObj.obj.removeEventListener)then
					listenerObj.obj:removeEventListener(listenerObj.type,listenerObj.callback)
					listeners[i] = nil
				end
			end
		end
	end
	
	local function addButtonAction(btn,callback)
		_G.addButtonAction(btn,callback,addListener)
	end
	
	local bg = display.newImage(localGroup,"img/Backgrounds/bg.jpg")
	
	bg.x = _W/2
	bg.y = _H/2
	
	if (_W>_H)then
		setScale(bg,_W/bg.width)
	else
		setScale(bg,_H/bg.height)
	end
	
	set_msgs_location(0,scaleGraphics*120)
	
	local gold_bar
	
	require("src.libs.superDesign").addCap(localGroup,show_category,addButtonAction)
	gold_bar = require("src.libs.superDesign").add_gold_bar(localGroup,function (e)
		_G.currentLayer = 1
		local shop = require("src.ui.shop.shopWindow").new(addListener, function ()gold_bar:manualUpdate() end)
		localGroup:insert(shop)
	end,addButtonAction)
	
	local frameMc = display.newGroup()
	
	local frameBg = display.newImage(frameMc,"img/1024_mirror_bg.png")
	setScale(frameBg,scaleGraphics*0.4)
	
	if (widthX(frameBg)>_W)then
		setScale(frameBg,_W/frameBg.width)
	end
	
	if (heightY(frameBg)>_H*0.5)then
		setScale(frameBg,_H*0.6/frameBg.height)
	end
	
	for i=-1,1,2 do
		local starsGfx = display.newImage(frameMc,"img/m2_window_stars.png")
		setScale(starsGfx,frameBg.xScale)
		starsGfx.x = i*frameBg.xScale*295
		starsGfx.xScale = starsGfx.xScale*i*-1
		starsGfx.y = frameBg.xScale*300
	end
	
	local topHat = display.newImage(frameMc,"img/1024_top_music.png")
	setScale(topHat,frameBg.xScale*0.88)
	topHat.y = -heightY(frameBg)/2+frameBg.xScale*150
	
	local hatTxt = display.newImage(frameMc,"img/text_music2.png")
	setScale(hatTxt,frameBg.xScale)
	hatTxt.x,hatTxt.y = topHat.x,topHat.y+topHat.xScale*70
	
	frameMc.x = _W/2
	frameMc.y = _H/2+scaleGraphics*50
	
	if (frameMc.y<_H/2-heightY(frameMc)/2-scaleGraphics*50)then
		frameMc.y=_H/2-heightY(frameMc)/2-scaleGraphics*50
	end
	
	local btns = {}
	
	local posDeltaX = 155
	local posDeltaY = 140
	
	local poses = {x={-frameBg.xScale*posDeltaX,frameBg.xScale*posDeltaX},y={-frameBg.xScale*posDeltaY,0,frameBg.xScale*posDeltaY}}
	
	for i=1,2 do
		for j=1,3 do
			local btn_grp = display.newGroup()
			local btn_bg = display.newImage(btn_grp,"img/Misc/boxSong.png")
			
			setScale(btn_bg,frameBg.xScale*0.9)
			btn_bg.xScale = frameBg.xScale*0.8*0.9
			
			local textField = display.newText({parent=btn_grp,text="WTF",x=0,y=0,width=widthX(btn_bg)*0.9,font=_G.gameFontText,fontSize=scaleGraphics*15,align="center"})
			textField:setTextColor(0,0,0,0.6)
			
			btn_grp.setText = function (text)
				textField.size = scaleGraphics*15
				textField.text = text
				while textField.height>heightY(btn_bg)*0.9 do
					textField.size = textField.size-1
				end
			end
			
			btn_grp.setText(getGameParam(data_obj["current_game"],"music",(i-1)*3+j))
			
			btn_grp.x = poses.x[i]
			btn_grp.y = poses.y[j]+frameBg.xScale*100
			addButtonAction(btn_bg,function ()
				data_obj["category"] = textField.text
				show_songs()
			end)
			table.insert(btns,btn_grp)
			frameMc:insert(btn_grp)
		end
	end
	
	local trigger = 0
	
	local function onRefresh()
		local success
		if (trigger>0)then
			showIndicator(true)
			currencyApi:purchase(data_obj["refreshCost"], function (data)
				showIndicator(false)
				success = not(data["isError"])
				print(success)
				gold_bar.updateValue(data)
				
				if not(success) and trigger > 0 then
					show_msg("Not enogh money")
					return
				end
				
				if (trigger>0)then
					audio.play(sounds_arr["cash"])
				end
				
				local elementsArr = {}
				
				for i=1,#_G.categoryList["music"] do
					elementsArr[i] = _G.categoryList["music"][i]
				end
				
				shuffleTable( elementsArr )
				
				for i=1,6 do
					btns[i].setText(elementsArr[i])
					setGameParam(data_obj["current_game"],"music",i,elementsArr[i])
				end
				
				
			end)
			
		else
			local elementsArr = {}
				
			for i=1,#_G.categoryList["music"] do
				elementsArr[i] = _G.categoryList["music"][i]
			end
			
			shuffleTable( elementsArr )
			
			for i=1,6 do
				btns[i].setText(elementsArr[i])
				setGameParam(data_obj["current_game"],"music",i,elementsArr[i])
			end
		end
		
		trigger = trigger+1
		
	end
	
	if getGameParam(data_obj["current_game"],"music",1)=="" then
		onRefresh()
	else
		trigger = 1
	end
	
	local refreshBtn = display.newImage(frameMc,"img/button_refresh.png")
	setScale(refreshBtn,frameBg.xScale*0.9)
	refreshBtn.y = frameBg.xScale*420
	addButtonAction(refreshBtn,onRefresh)
	
	localGroup:insert(frameMc)
	
	
	return localGroup

end
