module(..., package.seeall);

function new()

	local localGroup = display.newGroup()

	local buttons = {}

	local addHint
	
	local answerField
	
	local answer
	
	local removed = false
	
	local wordsNum
	local charsMax
	
	local updateHeaders
	
	data_obj["isWon"] = false

	local hints = {}
	
	touch_btns = true

	local txts = {}
	
	local gui_mc = display.newGroup()
	localGroup:insert(gui_mc)
	
	local listeners = {}
	
	local function addListener(obj,type,callback)
		local listenerObj = {obj=obj,type=type,callback=callback}
		obj:addEventListener(type,callback)
		table.insert(listeners,listenerObj)
	end


	function localGroup:removeAllListeners()
		if(localGroup._dead~=true)then
			localGroup._dead = true;
		end
		for i=1,#listeners do
			if (listeners[i]~=nil )then
				local listenerObj = listeners[i]
				if (listenerObj.obj and listenerObj.obj.removeEventListener)then
					listenerObj.obj:removeEventListener(listenerObj.type,listenerObj.callback)
					listeners[i] = nil
				end
			end
		end
	end
	
	local function addButtonAction(btn,callback)
		_G.addButtonAction(btn,callback,addListener)
	end
	
	local function getType(genre,answer)
		if (_G.listOfWords["movies"][genre]==nil)then
			return "song"
		end
		for i=1,#_G.listOfWords["movies"][genre] do
			if (_G.listOfWords["movies"][genre][i][1]==answer)then
				return "movie" 
			end
		end
		return "song"
	end
	
	local function onLoadData(data)
		showIndicator(false)
		if (data["code"]==200)then
			data_obj["currentGameData"] = json.decode(data["data"])
			hints = json.decode(mime.unb64(data_obj["currentGameData"]["task"]["answer"]))
			answer = hints[1]

			trace()
			answerField.placeholder = "WTF? Guess the "..getType(hints[2],answer)
			hints[1] = data_obj["currentGameData"]["task"]["mainHint"]
			addHint(hints[1])
			
			if (hints[2]=="none")then
				txts[2].x = scaleGraphics*10000
				--txts[3].x = scaleGraphics*10000
				txts[3].x = scaleGraphics*10000
				txts[4].x = scaleGraphics*10000
				txts[3].lock.alpha = 0
				txts[4].lock.alpha = 0
				--txts[4].lock.alpha = 0
			end
			
			wordsNum = #string_split(answer," ")
			charsMax = #string_split(clear_word(answer),"")
			updateHeaders(wordsNum,charsMax)
		else
			show_msg("Connection lost")
			show_cabinet()
		end
	end
	
	local function onChangeShortData(data)
		showIndicator(false)
		if (data["code"]==200)then
			doNothing(json.encode(data))
			local currentGameId = data_obj["current_game"]
			local currentShortData = data_obj["short_data"][currentGameId]
			local currentOpponentId = data_obj["opponent"][currentGameId]
			local opponentPushId = currentShortData[currentOpponentId]
			
			doNothing("opponent data got")
			
			local playerId = getPlayerId()
			local displayName = data_obj["playfub"]["account_data"]["AccountInfo"]["TitleInfo"]["DisplayName"]
			
			doNothing(displayName)
			
			doNothing("player data got")
			doNothing("sending notification...")
			
			if (data_obj["isWon"]) then
				sendNotification(opponentPushId,displayName.." has successfuly guessed \""..answer.."\"!")
			else
				sendNotification(opponentPushId,displayName.." didn't guess correctly. Winning streak over")
			end
			doNothing("notiffication sent successfuly!")
			
			currencyApi:addMoney(data_obj["delta_money"], doNothing)
			
			show_results()
		else
			data_obj["short_data"][data_obj["current_game"]] = json.decode(json.encode(data_obj["old_short_data"]))
			data_obj["delta_money"] = 0;
			show_msg("Can't send data")
		end
	end
	
	data_obj["old_short_data"] = json.decode(json.encode(data_obj["short_data"][data_obj["current_game"]]))
	
	local function onAnswer(_answer,_question,time)
		
		if (string.lower(_answer)==string.lower(_question))then
			data_obj["isWin"] = true
			show_msg("You have successfully guesed: "..answer)
			showIndicator(true)
			local currentShortData = data_obj["short_data"][data_obj["current_game"]]
			doNothing(json.encode(currentShortData))
			currentShortData["state"] = "default"
			currentShortData["points"] = 0
			currentShortData["streak"] = currentShortData["streak"] or 0
			
			currentShortData["points"] = getPoints(time)
			currentShortData["streak"] = currentShortData["streak"]+1
			currentShortData[getPlayerId()] = data_obj["push_id"]
			
			data_obj["isWon"] = true
			
			data_obj["delta_money"] = currentShortData["points"]
			
			data_obj["resultsData"] = {streak=currentShortData["streak"],time=time,points=currentShortData["points"],opponentId=data_obj["opponent"][data_obj["current_game"]]}
			
			audio.play(sounds_arr["correct"])
			
			banarumApi:sendShortGameData(data_obj["current_game"],currentShortData,onChangeShortData)
		else
			show_msg("You haven't guessed correctly")
			audio.play(sounds_arr["wrong"])
			
		end
	end
	
	local function onGiveUp(time)
		data_obj["delta_money"] = 0;
		data_obj["isWin"] = false
		show_msg("You havent guessed "..answer)
		showIndicator(true)
		local currentShortData = data_obj["short_data"][data_obj["current_game"]]
		doNothing(json.encode(currentShortData))
		currentShortData["state"] = "default"
		currentShortData["points"] = currentShortData["points"] or 0
		currentShortData["streak"] = currentShortData["streak"] or 0
		
		currentShortData["points"] = 0
		currentShortData["streak"] = 0
		currentShortData[getPlayerId()] = data_obj["push_id"]
		
		data_obj["resultsData"] = {streak=currentShortData["streak"],time=time,points=currentShortData["points"],opponentId=data_obj["opponent"][data_obj["current_game"]]}
		data_obj["answer"] = answer
		banarumApi:sendShortGameData(data_obj["current_game"],currentShortData,onChangeShortData)
	end
	
	local bg = display.newImage(localGroup,"img/Backgrounds/bg.jpg")
	
	bg.x = _W/2
	bg.y = _H/2
	
	if (_W>_H)then
		setScale(bg,_W/bg.width)
	else
		setScale(bg,_H/bg.height)
	end
	
	set_msgs_location(0,scaleGraphics*80)
	
	
	
	
	
	local border = superDesign.add_border(localGroup,scaleGraphics*300,scaleGraphics*300)
	border.x = _W/2
	border.y = scaleGraphics*150
	
	local giveupBtn = display.newGroup()
	
	local giveupBack = display.newImage(giveupBtn,"img/button_red.png")
	setScale(giveupBack,scaleGraphics*0.25)
	
	local giveupText = display.newImage(giveupBtn,"img/text_giveup.png")
	setScale(giveupText,scaleGraphics*0.25)
	
	giveupBtn.x = _W/2-scaleGraphics*300/2+widthX(giveupBack)/2+scaleGraphics*10
	giveupBtn.y = border.y-scaleGraphics*300/2+heightY(giveupBack)/2+scaleGraphics*10
	
	local gold_bar
	
	gold_bar = require("src.libs.superDesign").add_gold_bar(localGroup,function (e)
		_G.currentLayer = 1
		answerField.tx = answerField.x
		answerField.x = -_W
		local shop = require("src.ui.shop.shopWindow").new(addListener, function () gold_bar:manualUpdate();answerField.x = answerField.tx end)
		localGroup:insert(shop)
	end,addButtonAction, function (data)
		if (data["offline"]==true)then
			return
		end
		showIndicator(true)
		banarumApi:getGameData(data_obj["current_game"],onLoadData)
	end)
	
	gold_bar.y = giveupBtn.y+heightY(giveupBack)/2+scaleGraphics*10
	
	
	localGroup:insert(giveupBtn)
	
	local timeBar = superDesign.add_time_bar(localGroup)
	
	timeBar.x = _W/2+scaleGraphics*300/2-scaleGraphics*70
	timeBar.y = giveupBtn.y
	
	local answerGrp = display.newGroup()
	local answerBg = display.newImage(answerGrp,"img/answer_bg.png")
	setScale(answerBg,scaleGraphics*0.25)
	answerGrp.x = _W/2
	answerGrp.y = gold_bar.y+heightY(answerBg)/2+scaleGraphics*20
	
	local answerTxt = display.newText(answerGrp,"Answer (2 words): 0/18",0,0,_G.gameFontText,scaleGraphics*14)
	answerTxt:setFillColor(0)
	localGroup:insert(answerGrp)
	
	local hintsGrp = display.newGroup()
	
	local hintsHeadGrp = display.newGroup()
	
	local hintsHeadBottom = display.newGroup()
	
	local hintsHeadBg = display.newImage(hintsHeadBottom,"img/m4_inset_top1.png")
	setScale(hintsHeadBg,scaleGraphics*0.24)
	
	hintsHeadGrp:insert(hintsHeadBottom)
	
	hintsHeadGrp.setHintBg = function (num)
		hintsHeadBg:removeSelf()
		audio.play( sounds_arr["coin"] )
		hintsHeadBg = display.newImage(hintsHeadBottom,"img/m4_inset_top"..num..".png")
		setScale(hintsHeadBg,scaleGraphics*0.24)
	end
	
	
	
	local strings = {"WTF","Hint 1","Hint 2","Hint 3"}
	
	local currentHint = 1;
	
	
	
	for i=1,4 do		
		local txt = display.newText(hintsHeadGrp,strings[i],0,0,_G.gameFontText,scaleGraphics*15)
		txt:setFillColor(0)
		addListener(txt,"tap",function(e)
			if (currentHint==i-1)then
				
				showIndicator(true)
				currencyApi:purchase(data_obj["hintCost"], function (data)
					showIndicator(false)
					local success = not(data["isError"])
					gold_bar.updateValue(data)
					if (success)then
						addHint(hints[i])
						currentHint=i
						hintsHeadGrp.setHintBg(i)
						for i=1,#txts do
							if (txts[i].i<currentHint+2)then
								if (txts[i].lock)then
									txts[i].lock:removeSelf()
									txts[i].lock = nil
								end
							end
						end
					else
						show_msg("Can't buy item")
					end
				end)
			end
		end)
		
		txt.x = (i-2.5)*widthX(hintsHeadBg)/4
		
		if (i>2)then
			txt.lock = display.newImage(hintsHeadGrp,"img/m4_lock.png")
			setScale(txt.lock,scaleGraphics*0.24)
			txt.lock.x = txt.x
		end
		
		txt.i = i
		tinsert(txts,txt)
	end
	
	hintsHeadGrp.y = -scaleGraphics*65
	
	hintsGrp.y = answerGrp.y+scaleGraphics*100
	hintsGrp.x = _W/2
	hintsGrp:insert(hintsHeadGrp)
	
	local stime = system.getTimer();
	
	addListener(Runtime,"enterFrame",function ()
		timeBar:setValue(math.round((system.getTimer()-stime)/1000))
	end)
	
	
	local hintsBodyGrp = display.newGroup()
	
	local lastHint
	
	addHint = function(hintTxt)
		doNothing(hintTxt)
		local txt = display.newText({parent=hintsBodyGrp,text=hintTxt,x=0,y=0,width=scaleGraphics*230,font=_G.gameFontText,fontSize=scaleGraphics*15,align="center"})
		while txt.height>scaleGraphics*90 do
			txt.size = txt.size-1
		end
		txt:setFillColor(0)
		txt.anchorY = 0
		if (lastHint)then
			txt.y = lastHint.y+lastHint.height
		else
			txt.y = 0
		end
		lastHint = txt
		
	end
	
	updateHeaders = function (wordsNum,charsMax,chars)
		chars = chars or 0
		answerTxt.text = "Answer ("..wordsNum.." words): "..chars.."/"..charsMax
	end
	
	local hintsBodyBg = display.newImage(hintsBodyGrp,"img/answer_body.png")
	hintsBodyBg.xScale = scaleGraphics*0.24
	hintsBodyBg.anchorY = 0;
	hintsBodyGrp.y = hintsHeadGrp.y+heightY(hintsHeadBg)/2
	--hintsBodyBg.yScale = (-(hintsHeadGrp.y+heightY(hintsHeadBg)/2+scaleGraphics*30)+(scaleGraphics*300/2-scaleGraphics*20))/hintsBodyBg.height
	--hintsBodyGrp.y = scaleGraphics*20
	hintsBodyBg.yScale = scaleGraphics*140/hintsBodyBg.height
	
	local hintsMid = display.newImage(hintsBodyGrp,"img/answer_mid.png")
	hintsMid.y = heightY(hintsBodyBg)/2
	setScale(hintsMid,scaleGraphics*0.24)

	local hintsBottom = display.newImage(hintsBodyGrp,"img/answer_bottom.png")
	setScale(hintsBottom,scaleGraphics*0.24)
	hintsBottom.y = heightY(hintsBodyBg)+heightY(hintsBottom)/2
	
	hintsGrp:insert(hintsBodyGrp)
	
	localGroup:insert(hintsGrp)
	
	local answerInputGrp = display.newGroup()
	
	answerField = native.newTextField(_W/2-scaleGraphics*20,0,  (_W-scaleGraphics*50)*bolToNumber(_W-scaleGraphics*50<scaleGraphics*400)+(scaleGraphics*350)*bolToNumber(_W-scaleGraphics*50>=scaleGraphics*400)-scaleGraphics*50, scaleGraphics*30);
	answerInputGrp:insert(answerField)
	
	local function textListener( event )
		if ( event.phase == "editing" ) then
			if (#clear_word(event.text)>charsMax)then
				answerField.text = event.oldText
				event.text = event.oldText
			end
			updateHeaders(wordsNum,charsMax,#clear_word(event.text))
		end
	end
	
	addListener(answerField,"userInput",textListener)
	
	local buttonOk = display.newImage(answerInputGrp,"img/button_ok.png")
	setScale(buttonOk,scaleGraphics*0.20)
	
	buttonOk.x = answerField.x+answerField.width/2+widthX(buttonOk)/2+scaleGraphics*2
	answerInputGrp.y = scaleGraphics*300-scaleGraphics*10
	
	addButtonAction(buttonOk,function () onAnswer(clear_word(answerField.text),clear_word(answer),timeBar:getValue()) end)
	localGroup:insert(answerInputGrp)
	
	addButtonAction(giveupBack,function()
		onGiveUp(timeBar:getValue())
	end)
	
	
	return localGroup

end
