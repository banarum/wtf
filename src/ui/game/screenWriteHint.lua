module(..., package.seeall);

function new()

	local localGroup = display.newGroup()

	local buttons = {}

	local removed = false
	
	local answerField

	touch_btns = true

	local gui_mc = display.newGroup()
	localGroup:insert(gui_mc)
	
	local listeners = {}
	
	local function addListener(obj,type,callback)
		local listenerObj = {obj=obj,type=type,callback=callback}
		obj:addEventListener(type,callback)
		table.insert(listeners,listenerObj)
	end

	function localGroup:removeAllListeners()
		if(localGroup._dead~=true)then
			localGroup._dead = true;
		end
		for i=1,#listeners do
			if (listeners[i]~=nil)then
				local listenerObj = listeners[i]
				listenerObj.obj:removeEventListener(listenerObj.type,listenerObj.callback)
				listeners[i] = nil
			end
		end
	end
	
	local function addButtonAction(btn,callback)
		_G.addButtonAction(btn,callback,addListener)
	end
	
	local function onChangeShortData(data)
		showIndicator(false)
		if (data["code"]==200)then
			trace(json.encode(data))
			trace("short_data")
			local currentGameId = data_obj["current_game"]
			local currentShortData = data_obj["short_data"][currentGameId]
			local currentOpponentId = data_obj["opponent"][currentGameId]
			local opponentPushId = currentShortData[currentOpponentId]
			
			trace("opponent data got")
			
			local playerId = getPlayerId()
			local displayName = data_obj["playfub"]["account_data"]["AccountInfo"]["TitleInfo"]["DisplayName"]
			
			trace(displayName)
			
			trace("player data got")
			trace("sending notification...")
			sendNotification(opponentPushId,displayName.." sent a WTF to You!")
			trace("notiffication sent successfuly!")
			trace("showing cabinet...")
			show_cabinet()
			trace("cabinet is on screen!")
		else
			show_msg("Can't send data")
		end
	end
	
	local function onSent(data)
		
		if (data["code"]==200)then
			trace(json.encode(data))
			
			local currentShortData = data_obj["short_data"][data_obj["current_game"]]
			trace(json.encode(currentShortData))
			currentShortData["state"] = "answer"
			currentShortData[getPlayerId()] = data_obj["push_id"]
			
			banarumApi:sendShortGameData(data_obj["current_game"],currentShortData,onChangeShortData)
		else
			showIndicator(false)
			show_msg("Can't send data")
		end
	end
	
	local isChanged = false
	
	local function onComplete(hintTxt)
		if (isChanged==false) then
			isChanged = true
			for i=3,1,-1 do
				data_obj["element"][i+1] = data_obj["element"][i]
			end
			data_obj["element"][2] = data_obj["category"];
		end
		
		if (data_obj["element"][3]=="none")then
			data_obj["element"] = {answerField.text,"none","none","none"}
		end
		
		local task = 
		{
			answer = mime.b64(json.encode(data_obj["element"])),
			mainHint = hintTxt,
		}
		data_obj["currentGameData"].task = task
		setGameParam(data_obj["current_game"],nil,nil,nil)
		showIndicator(true)
		banarumApi:sendGameData(data_obj["current_game"],data_obj["currentGameData"],getPlayerId(),onSent)
	end
	
	
	local bg = display.newImage(localGroup,"img/Backgrounds/bg.jpg")
	
	bg.x = _W/2
	bg.y = _H/2
	
	if (_W>_H)then
		setScale(bg,_W/bg.width)
	else
		setScale(bg,_H/bg.height)
	end
	
	set_msgs_location(0,scaleGraphics*80)
	
	require("src.libs.superDesign").addCap(localGroup,data_obj["back"],addButtonAction)
	
	local frame_grp = display.newGroup()
	localGroup:insert(frame_grp)
	
	local txtPanel = display.newText({parent=localGroup,text="You are sending a WTF to "..login_obj.data[data_obj["games_data"][data_obj["current_game"]]["opponent_id"]]["UserInfo"]["TitleInfo"]["DisplayName"],x=0,y=0,width = scaleGraphics*300,height=0,font=_G.gameFontText,fontSize=scaleGraphics*20,align="center"})
	txtPanel:setTextColor(0)
	txtPanel.x = _W/2
	
	if (data_obj["element"][2] ~= "none")then
		txtPanel.text = txtPanel.text.." for \""..data_obj["element"][1].."\""
	end
	
	txtPanel.y = scaleGraphics*100+txtPanel.height/2
	
	local frame = require("src.libs.superDesign").add_border(frame_grp, txtPanel.width+scaleGraphics*30, txtPanel.height+scaleGraphics*20);
	
	
	frame.x = txtPanel.x
	frame.y = txtPanel.y
	
	local hintBox
	
	local textBg = display.newImage(localGroup,"img/text_delimiter.png")
	setScale(textBg,_W/textBg.width)
	textBg.yScale = _H/2/textBg.height
	textBg.x = _W/2
	textBg.anchorY = 0
	textBg.y = _H-heightY(textBg)
	textBg.alpha = 0.6
	
	localGroup:addEventListener("tap",function() native.setKeyboardFocus(nil) end)
	
	local btnOk = display.newImage(localGroup,"img/button_ok.png")
	setScale(btnOk,scaleGraphics*0.25)
	btnOk.x = _W/2
	btnOk.y = _H/2+scaleGraphics*210
	addButtonAction(btnOk,function ()
		if (#hintBox.text<5) then
			native.showAlert( "Submit", "Your hint should have 5 symbols or more", { "Ok" }, doNothing)
			return
		end
		onComplete(hintBox.text)
	end)
	
	local limitTxt = display.newText(localGroup,"0/140",0,0,_G.gameFontText,scaleGraphics*17)
	limitTxt:setTextColor(0)
	
	answerField = native.newTextField(-_W*2, _H/2+scaleGraphics*35, 200*scaleGraphics, 30*scaleGraphics);
	answerField.placeholder = "Answer"
	answerField.sy = answerField.y
	answerField.align = "center"
	localGroup:insert(answerField)
	

	
	if (data_obj["element"][2]=="none")then
		answerField.x = _W/2-scaleGraphics*30
	end
	
	
	
	local function onEdit()
		transition.to(hintBox,{time=200,y=scaleGraphics*250})
		transition.to(answerField,{time=200,y=scaleGraphics*250+(answerField.sy-hintBox.sy)})
		transition.to(textBg,{time=200,yScale=scaleGraphics,y=-scaleGraphics*50,alpha = 0.15})
		transition.to(btnOk,{time=200,y=scaleGraphics*40,x = _W-scaleGraphics*50})
		transition.to(limitTxt,{time=200,y=scaleGraphics*70,x = _W-scaleGraphics*50})
	end
	
	local function onReset()
		transition.to(hintBox,{time=200,y=hintBox.sy})
		transition.to(textBg,{time=200,yScale=_H/2/textBg.height,y=textBg.sy,alpha = 0.6})
		transition.to(btnOk,{time=200,y=btnOk.sy,x=btnOk.sx})
		transition.to(limitTxt,{time=200,y=limitTxt.sy,x=limitTxt.sx})
		transition.to(answerField,{time=200,y=answerField.sy})
	end

	local function textListener( event )

		if ( event.phase == "began" ) then
			onEdit()
		elseif ( event.phase == "ended" or event.phase == "submitted" ) then
			onReset()

		elseif ( event.phase == "editing" ) then
			if (#event.text>140)then
				hintBox.text = event.text:sub(1,140)
				event.text = event.text:sub(1,140)
			end
			limitTxt.text = #event.text.."/140"
			
		end
	end
	
	local function smallTextListener(event)
		if ( event.phase == "began" ) then
			onEdit()
		elseif ( event.phase == "ended" or event.phase == "submitted" ) then
			onReset()
		end
	end

	-- Create text box
	hintBox = native.newTextBox( _W/2, _H/2+scaleGraphics*120, scaleGraphics*280, scaleGraphics*130 )
	hintBox.size = scaleGraphics*15
	hintBox.text = ""
	hintBox.isEditable = true
	hintBox:addEventListener( "userInput", textListener )
	answerField:addEventListener( "userInput", smallTextListener )
	localGroup:insert(hintBox)
	
	limitTxt.y = hintBox.y-hintBox.height/2-limitTxt.height/2-scaleGraphics*10
	limitTxt.x = hintBox.x+hintBox.width/2-limitTxt.width/2
	
	
	hintBox.sy = hintBox.y
	btnOk.sy = btnOk.y
	btnOk.sx = btnOk.x
	
	limitTxt.sy = limitTxt.y
	limitTxt.sx = limitTxt.x
	textBg.sy = textBg.y
	
	
	return localGroup

end
