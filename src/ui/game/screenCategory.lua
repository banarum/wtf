module(..., package.seeall);

function new()

	local localGroup = display.newGroup()

	local buttons = {}

	local removed = false

	touch_btns = true

	local gui_mc = display.newGroup()
	localGroup:insert(gui_mc)
	
	local listeners = {}
	
	local function addListener(obj,type,callback)
		local listenerObj = {obj=obj,type=type,callback=callback}
		obj:addEventListener(type,callback)
		table.insert(listeners,listenerObj)
	end

	function localGroup:removeAllListeners()
		if(localGroup._dead~=true)then
			localGroup._dead = true;
		end
		for i=1,#listeners do
			if (listeners[i]~=nil)then
				local listenerObj = listeners[i]
				listenerObj.obj:removeEventListener(listenerObj.type,listenerObj.callback)
				listeners[i] = nil
			end
		end
	end
	
	local function addButtonAction(btn,callback)
		_G.addButtonAction(btn,callback,addListener)
	end
	
	local function onLoadData(data)
		showIndicator(false)
		if (data["code"]==200)then
			data_obj["currentGameData"] = data["data"]
			trace(data_obj["currentGameData"])
			if (data_obj["currentGameData"].state==nil)then
				data_obj["currentGameData"]=createGameData()
			end
		else
			show_msg("Connection lost")
			show_cabinet()
		end
	end
	
	local bg = display.newImage(localGroup,"img/Backgrounds/bg.jpg")
	
	bg.x = _W/2
	bg.y = _H/2
	
	if (_W>_H)then
		setScale(bg,_W/bg.width)
	else
		setScale(bg,_H/bg.height)
	end
	
	set_msgs_location(0,scaleGraphics*80)
	
	require("src.libs.superDesign").addCap(localGroup,show_cabinet,addButtonAction)
	
	local center = _H/2+scaleGraphics*50
	local delta = scaleGraphics*100
	
	local musicGrp = display.newGroup()
	
	local musicBg = display.newImage(musicGrp,"img/pick_song.png")
	setScale(musicBg,scaleGraphics*0.28)
	
	local musicText = display.newImage(musicGrp,"img/text_music.png")
	setScale(musicText,scaleGraphics*0.28)
	--musicText.y = scaleGraphics*20
	
	musicGrp.x = _W/2
	
	musicGrp.y = center-delta
	
	
	addButtonAction(musicBg,show_music)
	
	
	localGroup:insert(musicGrp)
	
	
	
	
	local filmsGrp = display.newGroup()
	
	local filmsBg = display.newImage(filmsGrp,"img/pick_movie.png")
	setScale(filmsBg,scaleGraphics*0.28)
	
	local filmsText = display.newImage(filmsGrp,"img/text_movie.png")
	setScale(filmsText,scaleGraphics*0.28)
	--filmsText.y = scaleGraphics*20
	
	filmsGrp.x = _W/2
	
	filmsGrp.y = center+delta
	
	addButtonAction(filmsBg,show_movie)
	
	localGroup:insert(filmsGrp)
	showIndicator(true)
	banarumApi:getGameData(data_obj["current_game"],onLoadData)
	
	
	return localGroup

end
