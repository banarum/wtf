module(..., package.seeall);

function new()

	local localGroup = display.newGroup()

	local buttons = {}

	local removed = false

	touch_btns = true

	local gui_mc = display.newGroup()
	localGroup:insert(gui_mc)

	local listeners = {}
	
	local function addListener(obj,type,callback)
		local listenerObj = {obj=obj,type=type,callback=callback}
		obj:addEventListener(type,callback)
		table.insert(listeners,listenerObj)
	end

	function localGroup:removeAllListeners()
		if(localGroup._dead~=true)then
			localGroup._dead = true;
		end
		
	end
	
	function localGroup:onFade()
		for i=1,#listeners do
			if (listeners[i]~=nil)then
				local listenerObj = listeners[i]
				listenerObj.obj:removeEventListener(listenerObj.type,listenerObj.callback)
				listeners[i] = nil
			end
		end
	end
	
	local bg = display.newImage(localGroup,"img/Backgrounds/bg.jpg")
	
	bg.x = _W/2
	bg.y = _H/2
	
	if (_W>_H)then
		setScale(bg,_W/bg.width)
	else
		setScale(bg,_H/bg.height)
	end
	
	local function onAccountInfo(data)
		showIndicator(false)
		if (data["code"]==200)then
			data_obj["playfub"]["account_data"] = data["data"]
			data_obj["login_type"] = "fb"
			login_obj["login_type"] = "fb"
			show_cabinet()
		else
			
		end
	end
	
	notifications.registerForPushNotifications()
	
	local function onPlayfubNameChange(data)
		
		if (data["code"]==200)then
			playfubApi:getAccountInfo(data_obj["playfub"]["PlayFabId"],onAccountInfo)
			
		else
			showIndicator(false)
		end
	end
	
	local function onPlayfubLogin(data)
		if (data["code"]==200)then
			show_msg("Playfub linked")
			data_obj["playfub"] = data["data"]
			playfubApi:changeDisplayName(login_obj["fb_name"],onPlayfubNameChange)
		else
			showIndicator(false)
			show_msg("Failed to link Playfub")
		end
	end
	
	local function onFbData(data)
		if (data["code"]==200)then
			playfubApi:loginFbUser(login_obj["fb_token"],onPlayfubLogin)
		else
			showIndicator(false)
			show_msg("Cant reach Facebook Data")
		end
	end
	
	local function onFbLogin(data)
		show_msg(data.message)
		if (data["code"]==200)then
			facebookApi:getUserData(onFbData)
		else
			showIndicator(false)
		end
		
	end
	
	set_msgs_location(0,scaleGraphics*200)
	
	local function intro()
		local mc = display.newGroup();
		mc.xScale, mc.yScale = scaleGraphics/4, scaleGraphics/4;
		localGroup:insert(mc);
		
		mc.x, mc.y = _W/2-20*scaleGraphics, 100*scaleGraphics;
		
		local function add_letter(val, delay, tx)
			local l = display.newImage(mc, "img/"..val..".png");
			l.x = tx
			transition.from(l, {delay=delay, time=600, alpha=0, xScale=4, yScale=4, transition=easing.inQuad});
		end
		
		local delay=300;
		add_letter("w",  delay*0, -260);
		add_letter("t",  delay*1, 0);
		add_letter("f",  delay*2, 260);
		add_letter("q",  delay*3, 260+260);

	end
	intro();
	
	local function addButtonAction(btn,callback)
		_G.addButtonAction(btn,callback,addListener)
	end
	
	local loginEmailBtn = display.newGroup()
	local loginEmailBg = display.newImage(loginEmailBtn,"img/button_long_green.png")
	setScale(loginEmailBg,scaleGraphics*0.25)
	local loginEmailTxt = display.newImage(loginEmailBtn,"img/text_login.png")
	setScale(loginEmailTxt,scaleGraphics*0.25)
	local loginEmailIco = display.newImage(loginEmailBtn,"img/ico_email.png")
	setScale(loginEmailIco,scaleGraphics*0.22)
	loginEmailIco.x = -scaleGraphics*55
	loginEmailBtn.x = _W/2
	loginEmailBtn.y = _H/2-scaleGraphics*30
	addButtonAction(loginEmailBg,show_login)
	localGroup:insert(loginEmailBtn)
	
	local loginFbBtn = display.newGroup()
	local loginFbBg = display.newImage(loginFbBtn,"img/button_long_blue.png")
	setScale(loginFbBg,scaleGraphics*0.25)
	local loginFbTxt = display.newImage(loginFbBtn,"img/text_login.png")
	setScale(loginFbTxt,scaleGraphics*0.25)
	local loginFbIco = display.newImage(loginFbBtn,"img/ico_fb.png")
	setScale(loginFbIco,scaleGraphics*0.22)
	loginFbIco.x = -scaleGraphics*55
	loginFbBtn.x = _W/2
	loginFbBtn.y = _H/2+scaleGraphics*30
	addButtonAction(loginFbBg,function () showIndicator(true);facebookApi:loginUser(onFbLogin) end)
	localGroup:insert(loginFbBtn)
	
	if (login_obj["autologin"])then
		if (login_obj["login_type"]=="fb")then
			showIndicator(true)
			facebookApi:loginUser(onFbLogin)
		else
			show_login()
		end
	end
	
	
	return localGroup

end
