module(..., package.seeall);

function init()
	local c = {}
	local o = {}
	c.spread_sheets = {["data"] = "1Gvcnt3wEY0EZwpaschhMyVMbRlDqw-XBwLsyHYi4xuM"}
	c.spread_sheets_ground = {["bottom"] = "https://docs.google.com/spreadsheets/u/0/d/",["top"] = "/export?format=",["format_tsv"] = "tsv"}
	
	function o:requestData()
		
		local success = c.constructPrms(system.DocumentsDirectory)

		if (success==nil)then
			c.constructPrms(system.ResourceDirectory)
		end
		
		c.perform_tsv_download(c.onData)
	end
	
	c.constructPrms = function(dir)
		c.categoryList = {["music"]={},["movies"]={}}
		c.listOfWords = {["music"]={},["movies"]={}}

		local data = loadFile("data/wtf_table.tsv", dir)
		if (data==nil)then
			return nil
		end
		local data_arr = {}

		local strings_arr = string_split(data,"\n")
		local table_str = ""
		for i=1,#strings_arr do
			local sub_str = strings_arr[i]
			local sub_str_arr = string_split(sub_str,"	")
			data_arr[i] = {}
			for j=1,#sub_str do
				data_arr[i][j] = sub_str_arr[j]
			end
		end
		
		

		for i=2,#data_arr do
			local sub_arr = data_arr[i]
			if sub_arr[2]~="" then
				local txt = clear_text_from(sub_arr[2]," ")
				table.insert(c.categoryList["music"],txt)
				c.listOfWords["music"][txt] = {}
			end
			
			if sub_arr[4]~="" then
				local txt = clear_text_from(sub_arr[4]," ")
				table.insert(c.categoryList["movies"],txt)
				c.listOfWords["movies"][txt] = {}
			end
		end

		
		for i=2,#data_arr do
			local sub_arr = data_arr[i]
			local txt = clear_text_from(sub_arr[9]," ")
			if sub_arr[6]~="" and sub_arr[9]~="" and sub_arr[7]~="" and sub_arr[8]~="" and c.listOfWords["music"][txt] then
				table.insert(c.listOfWords["music"][txt],{sub_arr[6],sub_arr[7],sub_arr[8]})
			end
			
			local txt = clear_text_from(sub_arr[14]," ")
			if sub_arr[11]~="" and sub_arr[14]~="" and sub_arr[12]~="" and sub_arr[13]~="" and c.listOfWords["movies"][txt] then
				table.insert(c.listOfWords["movies"][txt],{sub_arr[11],sub_arr[12],sub_arr[13]})
			
			end
			
		end
		
		_G.categoryList = c.categoryList
		_G.listOfWords = c.listOfWords
		
		return true
	end
	
	c.perform_tsv_download = function(callback)
		network.download(
			c.spread_sheets_ground["bottom"]..c.spread_sheets["data"]..c.spread_sheets_ground["top"]..c.spread_sheets_ground["format_tsv"],
			"GET",
			callback,
			params,
			"temp_wtf_table.tsv",
			system.TemporaryDirectory
		)
	end
	
	
	local tries = 0

	c.onData = function ( event )
		if ( event.isError ) then
			tries = tries+1
			
			if (tries<3)then
				c.perform_tsv_download(c.onData)
			end
			trace( "Network error - download failed: ", event.response )
		elseif ( event.phase == "began" ) then
			trace( "Progress Phase: began" )
		elseif ( event.phase == "ended" ) then
			trace("ended")
			local data = loadFile(event.response.filename, event.response.baseDirectory)
			if (data==nil)then
				return nil
			end
			local data_arr = {}

			local strings_arr = string_split(data,"\n")
			for i=1,#strings_arr do
				local sub_str = strings_arr[i]
				local sub_str_arr = string_split(sub_str,"	")
				data_arr[i] = {}
				for j=1,#sub_str do
					data_arr[i][j] = sub_str_arr[j]
				end
			end
			if data_arr[2][1]=="1" then
				trace("success")
				createSubFolder("","data")
				copyFile( event.response.filename, event.response.baseDirectory, "data/wtf_table.tsv", system.DocumentsDirectory, true )
				local success = c.constructPrms(system.DocumentsDirectory)
				if (success==nil)then
					c.constructPrms(system.ResourceDirectory)
				end
			else
				trace("fail")
			end
			
			deleteFile(event.response.filename,event.response.baseDirectory)
			
		end
		
		
	end
	
	return o
end