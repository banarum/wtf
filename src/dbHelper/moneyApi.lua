module( ..., package.seeall )

function init()
	local o = {}
	local c = {}
	
	function o:initMoney(val)
		c.key = data_obj["playfub"]["PlayFabId"]
		if (login_obj[c.key]==nil)then
			login_obj[c.key] = {["money"]=val}
		end
	end
	
	function o:getMoney()
		return login_obj[c.key]["money"]
	end
	
	function c:setMoney(val)
		login_obj[c.key]["money"] = val
	end
	
	function o:testPurchase(val)
		if (o:getMoney()-val>=0)then
			return true
		end
		return false
	end
	
	function o:purchaseItem(val)
		if (o:testPurchase(val))then
			local currentMoney = o:getMoney()
			c:setMoney(currentMoney - val)
			return true
		end
		return false
	end
	
	function o:addMoney(val)
		c:setMoney(o:getMoney()+val)
	end
	
	return o
end
