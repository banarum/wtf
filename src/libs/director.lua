module( ..., package.seeall )

function init()
	local o = {}
	local c = {}
	c.old_mc = nil
	o.changeScene = function(prs, moduleName)
		native.setKeyboardFocus( nil )
		if(moduleName==nil)then
			moduleName = prs;
			prs = nil;
		end
		if(c.old_mc)then
			if(c.old_mc.removeAllListeners)then
				c.old_mc:removeAllListeners()
				c.old_mc.removeAllListeners = nil;
			end
			if(c.old_mc.removeAll)then
				c.old_mc:removeAll();
				c.old_mc.removeAll = nil;
			end
			c.old_mc:removeSelf();
			c.old_mc = nil;
		end
		c.old_mc = require(moduleName).new(prs);
		mainGroup.parent:insert(1, c.old_mc);
		return c.old_mc
	end
	
	o.getCurrHandler = function()
		return c.old_mc;
	end
	
	return o
end
