module(..., package.seeall);

function init()
	_G._W = display.contentWidth;
	_G._H = display.contentHeight;

	_G.scaleDevice = 1;
	_G.scaleGraphics = 1;

	local min_wh = math.min(_W, _H);
	if(min_wh>=450)then
		_G.scaleGraphics = 1.5;
	end
	if(min_wh>=480)then
		_G.scaleGraphics = 1.6;
	end
	if(min_wh>=600)then
		_G.scaleGraphics = 2;
	end
	if(min_wh>=800)then
		_G.scaleGraphics = 2.5;
	end
	if(min_wh>=1000)then
		_G.scaleGraphics = 3;
	end
	if(min_wh>=1100)then
		_G.scaleGraphics = 3.5;
	end
	if(min_wh>=1500)then
		_G.scaleGraphics = 4;
	end
	
	if(min_wh>=1600)then
		_G.scaleGraphics = math.floor((_W/500)*2)/2;
	end
	
	local dpi = system.getInfo("androidDisplayApproximateDpi");
	
	if(_W..'x'.._H=='2048x1536')then--ipad retina
		_G.scaleGraphics = 4;
	end


	_G.scaleGraphics = _G.scaleGraphics*_G.scaleDevice;


	trace('_device_original:',_W,_H);
	if(optionsBuild == "amazon")then
		if(_H == 1200)then
			_H = _H - 40;
		elseif(_H == 800)then
			_H = _H - 30;
		else
			_H = _H - 20;
		end
	end
	trace('_device_fixed:',_W,_H)
	trace('_scales:',_G.scaleGraphics,_G.scaleDevice)
	trace("_model:"..system.getInfo("platformName")..','..system.getInfo("model")..','..optionsBuild);
end

function game()
	local zoom_val = 1;
	local function zoom(val)
		zoom_val = val;
	end
	if(_H==540)then
		zoom(0.80);
	elseif(_H==480)then
		zoom(0.75);
	elseif(_H>1500)then
		zoom(2.25);
	elseif(_H>1070)then
		zoom(1.5);
	elseif(_H>670)then
		zoom(1.10);
	elseif(_H<480)then
		if(_H>400)then
			zoom(0.75);
		else
			zoom(0.5);
		end
	else
		zoom(1.0);
	end
	
	local dpi = system.getInfo("androidDisplayApproximateDpi");
	--dpi = 640;--test value
	if(dpi)then
		--show_msg('dpi: '..tostring(dpi).."x"..tostring(scaleGraphics));--scaleGraphics
		if(dpi>=640)then
			zoom(1.75);
		elseif(dpi>=480)then
			zoom(1.5);
		end
		--240 vs 2
		--ldpi	120
		--mdpi	160
		--hdpi	240
		--xhdpi	320
		--xxhdpi	480
		--xxxhdpi	640
		--tvdpi	213
	end

	if(options_console)then
		local min_wh = math.min(_W, _H);
		if(min_wh == 720)then
			zoom(1);
		end
		if(min_wh == 1080)then
			zoom(1.5);
		end
	end
	
	return zoom_val;
end