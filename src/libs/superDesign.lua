module(..., package.seeall);

function add_bg(tar)
	local background = display.newImage(tar, "img/Backgrounds/bg.jpg");
	background.x, background.y = _W/2, _H/2;
	local this_scale = math.max(_W/background.width, _H/background.height);
	background.xScale = this_scale;
	background.yScale = this_scale;
	
	return background,this_scale;
end

function addCap(tar,callback,btnCallback)
	local defaultCap = display.newGroup()
	local backTape = display.newImage(defaultCap,"img/top_tape.png")
	setScale(backTape,scaleGraphics*0.3)
	backTape.x = widthX(backTape)/2
	
	local frontBtn = display.newImage(defaultCap,"img/top_button_back.png")
	setScale(frontBtn,scaleGraphics*0.34)
	frontBtn.x = backTape.x
	frontBtn.y = backTape.y-scaleGraphics*4
	
	local wtfLogo = display.newImage(defaultCap,"img/wtf_logo.png")
	setScale(wtfLogo,scaleGraphics*0.25)
	wtfLogo.x = _W/2
	
	btnCallback(frontBtn,callback)
	
	defaultCap.y = heightY(backTape)/2
	
	tar:insert(defaultCap)
end

function addCabinetCap(tar,callbackSettings,callbackLeader,btnCallback)
	local defaultCap = display.newGroup()
	local frontTape = display.newImage(defaultCap,"img/top_tape.png")
	setScale(frontTape,scaleGraphics*0.3)
	frontTape.x = widthX(frontTape)/2
	
	local frontBtn = display.newImage(defaultCap,"img/top_button_settings.png")
	setScale(frontBtn,scaleGraphics*0.34)
	frontBtn.x = frontTape.x
	frontBtn.y = frontTape.y-scaleGraphics*4
	
	local wtfLogo = display.newImage(defaultCap,"img/wtf_logo.png")
	setScale(wtfLogo,scaleGraphics*0.25)
	wtfLogo.x = _W/2
	
	btnCallback(frontBtn,callbackSettings)
	
	defaultCap.y = heightY(frontTape)/2
	
	tar:insert(defaultCap)
end

function add_gold_bar(tar,callbackAdd,btnCallback, updateCallback)
	local mc = display.newGroup();
	mc.x, mc.y = _W/2, 80*scaleGraphics;
	tar:insert(mc);
	local bg = display.newImage(mc, "img/gold/gold_bg.png");
	bg.xScale,bg.yScale = scaleGraphics/4,scaleGraphics/4;
	
	local dtxt = display.newText(mc, "0", 0, 0, _G.gameFontText, 19*scaleGraphics);
	dtxt:setTextColor(0.2);
	
	mc.txt_field = dtxt
	
	function mc.updateValue(data)
		if (data["value"])then
			print("Value: "..data["value"].."(curr val: "..dtxt.text..")");
			local currentCoins = data["value"]
			dtxt.text = currentCoins
		end
	end
	
	function mc:manualUpdate(self)
		print("Calling back started!");
		updateGoldValue(function (data)
			print("Calling back succeed");
			mc.updateValue(data)
		end)
	end	
	
	updateGoldValue(function (data)
		mc.updateValue(data)
		if (updateCallback)then
			updateCallback(data)
		end
	end)
	
	local ico = display.newImage(mc, "img/gold/gold_coin.png");
	ico.xScale,ico.yScale = scaleGraphics/5,scaleGraphics/5;
	ico.x = -50*scaleGraphics;
	local btn = display.newImage(mc, "img/gold/gold_button.png");
	btn.xScale,btn.yScale = scaleGraphics/4,scaleGraphics/4;
	btn.x = -ico.x;
	btnCallback(btn,callbackAdd)
	return mc
end

function add_time_bar(tar)
	local mc = display.newGroup();
	mc.x, mc.y = _W/2, 80*scaleGraphics;
	tar:insert(mc);
	local bg = display.newImage(mc, "img/gold/gold_bg.png");
	bg.xScale,bg.yScale = scaleGraphics/4,scaleGraphics/4;
	
	local dtxt = display.newText(mc, "0", 0, 0, _G.gameFontText, 19*scaleGraphics);
	dtxt:setTextColor(0/255,73/255,125/255);
	
	mc.txt_field = dtxt
	
	local val = 0;
	
	function mc:setValue(_val)
		val = _val
		dtxt.text = val
	end
	
	function mc:getValue()
		return val
	end
	
	local ico = display.newImage(mc, "img/clock.png");
	ico.xScale,ico.yScale = scaleGraphics/4,scaleGraphics/4;
	ico.x = 50*scaleGraphics;

	return mc
end

function add_border(tar,w,h)
	local save = 44;
	local wm,hm = w-save*2, h-save*2;
	local mc = display.newGroup();
	mc.w,mc.h = w/2,h/2;
	tar:insert(mc);
	
	local body5 = display.newImage(mc, "img/bg_95.png");
	body5.width, body5.height = wm, hm;
	
	local body1 = display.newImage(mc, "img/bg_91.png");
	body1.xScale,body1.yScale = 1,1;
	body1.x, body1.y = -(wm+body1.width)/2, -(hm+body1.height)/2;
	local body1 = display.newImage(mc, "img/bg_91.png");
	body1.xScale,body1.yScale = -1,1;
	body1.x, body1.y = (wm+body1.width)/2, -(hm+body1.height)/2;
	
	local body1 = display.newImage(mc, "img/bg_97.png");
	body1.xScale,body1.yScale = 1,1;
	body1.x, body1.y = -(wm+body1.width)/2, (hm+body1.height)/2;
	local body1 = display.newImage(mc, "img/bg_97.png");
	body1.xScale,body1.yScale = -1,1;
	body1.x, body1.y = (wm+body1.width)/2, (hm+body1.height)/2;
	
	local body2 = display.newImage(mc, "img/bg_92.png");
	body2.width = wm;
	body2.x,body2.y = 0,-(hm+body2.height)/2;
	local body2 = display.newImage(mc, "img/bg_98.png");
	body2.width = wm;
	body2.x,body2.y = 0,(hm+body2.height)/2;
	
	local body4 = display.newImage(mc, "img/bg_94.png");
	body4.height = hm;
	body4.x,body4.y = -(wm+body4.width)/2,0;
	local body4 = display.newImage(mc, "img/bg_94.png");
	body4.height = hm;
	body4.xScale = -1;
	body4.x,body4.y = (wm+body4.width)/2,0;
	
	return mc;
end


function add_mirror(group,scl)
	local grp = display.newGroup()
	local mirrormc = display.newImage(grp, "img/1024_mirror_bg.png");
	mirrormc.xScale, mirrormc.yScale = scaleGraphics/3,scaleGraphics/3;
	grp.x, grp.y = _W/2, _H/2 + 60*scaleGraphics;
	local hatmc = display.newImage(grp, "img/1024_top_".._G.currentGame.categoryChosen..".png");
	hatmc.xScale, hatmc.yScale = mirrormc.xScale*0.87, mirrormc.yScale*0.87;
	hatmc.x, hatmc.y = 0,mirrormc.y-410*mirrormc.yScale;
	
	local starsmc = display.newImage(grp, "img/m2_window_stars.png");
	starsmc.xScale, starsmc.yScale = mirrormc.xScale, mirrormc.yScale;
	starsmc.x,starsmc.y = -290*mirrormc.xScale, mirrormc.y+300*mirrormc.xScale;
	local starsmc = display.newImage(grp, "img/m2_window_stars.png");
	starsmc.xScale, starsmc.yScale = -mirrormc.xScale, mirrormc.yScale;
	starsmc.x,starsmc.y = 290*mirrormc.xScale, mirrormc.y+300*mirrormc.xScale;
	
	group:insert(grp)
	setScale(grp,sclx*990/widthX(mirrormc))
	grp.mirror = mirrormc
	grp.hatmc = hatmc
	return grp;
end